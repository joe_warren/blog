{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
module Stats
( renderYearsChart
, renderMonthsChart
, renderTagsChart
) where
import Hakyll (Compiler, Item, makeItem, recompilingUnsafeCompiler)
import qualified Graphics.Rendering.Chart.Easy as Chart
import qualified Graphics.Rendering.Chart.Backend.Diagrams as Chart
import qualified Graphics.Rendering.Chart.Backend.Types as Chart
import qualified Diagrams.Core.V as Diagrams
import qualified Data.List.NonEmpty as NE
import Control.Arrow ((&&&))
import Linear (V2 (..))
import qualified Diagrams.Core as Diagrams
import qualified Diagrams.Backend.SVG as Diagrams.SVG
import qualified Diagrams.TwoD
import qualified Diagrams.TwoD.Arc as Arc
import qualified Diagrams.TwoD.Text
import Diagrams.Direction (direction, fromDirection)
import Diagrams.Angle (turn, (@@))
import qualified Diagrams.Backend.SVG as Diagrams
import Diagrams.Combinators (cat', sep)
import Diagrams.Attributes (lineWidth)
import Diagrams.Util (with)
import Diagrams.Envelope (pad)
import qualified Graphics.Svg.Core as SVG
import qualified Data.Text.Lazy as TL
import Control.Lens (set, iset, itraversed, over, (&), (.~), (.>), (<.), _1, _2)
import Hakyll.Core.Compiler (unsafeCompiler)
import qualified Graphics.Rendering.Chart.Easy as Colours
import Control.Lens.Operators ((.=))
import qualified Graphics.Rendering.Chart as Chart
import Data.Monoid (Any)
import Data.Time.Format (defaultTimeLocale, months)
import Data.Time (UTCTime, utctDay)
import Data.Time.Calendar.OrdinalDate (pattern YearDay)
import Data.Time.Calendar.Month (pattern MonthDay, pattern YearMonth)
import Data.Bifunctor (second)
import Graphics.Rendering.Chart (PlotValue(fromValue))
import Graphics.Rendering.Chart.Axis.Indexed (addIndexes)
import Data.Aeson (Value(String))
import Data.Foldable (toList)

renderYearsChart :: String -> [UTCTime] -> Compiler (Item String)
renderYearsChart title times = do
    let years = (\(YearDay year _) -> year) . utctDay <$> times
        yearCounts = fmap (NE.head &&& (pure . length)) .  NE.group $ years 
        maxYear = maximum $ fst <$> yearCounts
        -- Adding an extra empty bar stops the current year from being truncated 
        yearCounts' = yearCounts <> [(maxYear +1, [0])] 
        graph =  Chart.plot $ do 
            Chart.layout_title .= title
            bars <- Chart.bars ["posts"] yearCounts'
            bars
                & set Chart.plot_bars_spacing (Chart.BarsFixGap 0 0) 
                & set Chart.plot_bars_alignment Chart.BarsLeft
                & set (Chart.plot_bars_item_styles . traverse . _2) Nothing 
                & set (Chart.plot_bars_item_styles . traverse . _1 . Chart.fill_color) (Chart.withOpacity Colours.maroon 1.0)
                & Chart.plotBars
                & return
    env <- unsafeCompiler $ Chart.defaultEnv Chart.vectorAlignmentFns (800 :: Double) 600
    let (diagram, _) = Chart.runBackendR env (Chart.toRenderable graph)
        svg = Diagrams.renderDia Diagrams.SVG.SVG (Diagrams.SVG.SVGOptions (Diagrams.TwoD.dims2D 800 600) Nothing "" [] False ) diagram
    makeItem (TL.unpack . SVG.renderText $ svg)
    
renderTagsChart :: String -> [(String, [a])]  -> Compiler (Item String)
renderTagsChart title tags = do
    let counts = pure . length . snd <$> tags
        setLabel :: Int -> String
        setLabel i = "tag:" <> (foldMap fst $ tags !? i)

        graph =  Chart.plot $ do 
            Chart.layout_title .= title
            bars <- Chart.bars ["posts"] (addIndexes counts)
            bars
                -- & set (Chart.plot_bars_item_styles . traverse . _2) Nothing 
                & set (Chart.plot_bars_item_styles . traverse . _1 . Chart.fill_color) (Chart.withOpacity Colours.maroon 1.0)
                & iset (Chart.plot_bars_values_with_labels .> itraversed <. (_2 . traverse . _2)) setLabel
                & Chart.plotBars
                & return
    env <- unsafeCompiler $ Chart.defaultEnv Chart.vectorAlignmentFns (800 :: Double) 600
    let (diagram, _) = Chart.runBackendR env (Chart.toRenderable graph)
        svg = Diagrams.renderDia Diagrams.SVG.SVG (Diagrams.SVG.SVGOptions (Diagrams.TwoD.dims2D 800 600) Nothing "" [] False ) diagram
    makeItem (TL.unpack . SVG.renderText $ svg)


headOption :: [a] -> Maybe a
headOption (x:_) = Just x
headOption _ = Nothing

(!?) :: [a] -> Int -> Maybe a
(!?) l i = headOption . drop i $ l

monthNames :: [String]
monthNames = fmap fst . months $ defaultTimeLocale
    
monthName :: Int -> String
monthName = mconcat . toList . (monthNames !?) . subtract 1

renderMonthsChart :: String -> [UTCTime] -> Compiler (Item String)
renderMonthsChart title times = do
    let months = (\(MonthDay (YearMonth _ month) _) -> month) . utctDay <$> times
        monthCounts i = length . filter (== i) $ months
        maxMonth = maximum $ monthCounts <$> [1..12]
        offset = 5
        innerR = 20 * fromIntegral offset / fromIntegral (offset + maxMonth)
        wedges = mconcat [
            let extent = 20 * fromIntegral (offset + monthCounts i) / fromIntegral (offset + maxMonth)
                a = (1/12) @@ turn
                d = Diagrams.TwoD.rotate ((fromIntegral (-i) / 12) @@ turn) (direction (V2 0 1))
                d' = fromDirection $ 
                        Diagrams.TwoD.rotate ((1 / 24) @@ turn) $
                        Diagrams.TwoD.scale extent d
            in cat' d' (with & sep .~ 0.5)
                [ lineWidth 0 $ Diagrams.TwoD.fillColor Colours.maroon (Arc.annularWedge extent innerR d a)
                , 
                    Diagrams.TwoD.rotate ((1 / 24) @@ turn) $
                    Diagrams.TwoD.rotate ((fromIntegral (-i) / 12) @@ turn) $
                    Diagrams.TwoD.text (monthName i)
                ]
            | i <- [1..12], monthCounts i > 0]
        emptyCircle = lineWidth 0 $ Diagrams.TwoD.circle 0.1
        diagram :: Diagrams.Diagram Diagrams.SVG 
        diagram = 
            Diagrams.TwoD.Text.font "sans-serif"
            Diagrams.TwoD.vsep 2
                [ emptyCircle
                , Diagrams.TwoD.Text.bold $ 
                    Diagrams.TwoD.Text.fontSize 14 $ 
                    Diagrams.TwoD.text title
                , wedges <> Diagrams.TwoD.circle innerR
                , emptyCircle
                ]
        svg = Diagrams.renderDia Diagrams.SVG.SVG (Diagrams.SVG.SVGOptions (Diagrams.TwoD.dims2D 800 600) Nothing "" [] False ) diagram
    makeItem (TL.unpack . SVG.renderText $ svg)
