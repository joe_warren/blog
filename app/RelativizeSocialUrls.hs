module RelativizeSocialUrls
( relativizeSocialUrls
) where

import           Data.List            (isPrefixOf)
import           Hakyll.Core.Compiler
import           Hakyll.Core.Item
import           Hakyll.Web.Html
import           Data.List                       (isPrefixOf)
import qualified Text.HTML.TagSoup               as TS

relativizeSocialUrls :: Item String -> Compiler (Item String)
relativizeSocialUrls item = do
    route <- getRoute $ itemIdentifier item
    return $ case route of
        Nothing -> item
        Just r  -> fmap (relativizeSocialUrlsWith $ toSiteRoot r) item

--------------------------------------------------------------------------------
-- | Relativize Social Media link URL's in HTML
relativizeSocialUrlsWith :: String  -- ^ Path to the site root
                   -> String  -- ^ HTML to relativize
                   -> String  -- ^ Resulting HTML
relativizeSocialUrlsWith root = withSocialUrls rel
  where
    isRel x = "/" `isPrefixOf` x && not ("//" `isPrefixOf` x)
    rel x   = if isRel x then root ++ x else x

-- | Apply a function to each Social URL on a webpage
withSocialUrls :: (String -> String) -> String -> String
withSocialUrls f = withTags tag
  where
    tag (tag@(TS.TagOpen s a)) | isSocialTag tag = TS.TagOpen s $ map attr a
    tag x                                        = x
    attr (k, v)                = (k, if isSocialUrlAttribute k then f v else v)


isSocialTag :: TS.Tag String -> Bool
isSocialTag (TS.TagOpen "meta" attrs) =
     ((lookup "property" attrs) `elem` (Just <$> ["og:image"])) ||
     ((lookup "name" attrs) `elem` (Just <$> ["twitter:image"]))
isSocialTag x = False


isSocialUrlAttribute :: String -> Bool
isSocialUrlAttribute = (`elem` ["content"])
