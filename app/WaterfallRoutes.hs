{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE InstanceSigs #-}
module WaterfallRoutes (waterfallRoutes, loadGlbFiles) where

import qualified Text.Pandoc as P
import qualified Data.Text as T
import           Hakyll
import Data.Functor ((<&>))
import Data.List (sort, sortOn, isPrefixOf, lines, intercalate)
import System.FilePath (takeFileName, (</>),takeExtension, replaceExtensions)
import System.Random (randomIO)
import System.Directory (createDirectoryIfMissing, doesFileExist, getCurrentDirectory)
import qualified Hakyll.Core.Store             as Store
import System.Process (readProcess)
import Data.Typeable (Typeable)
import Data.Binary (Binary)
import Control.Monad (forM_, (<=<))
import Data.Foldable (traverse_)
import Linear (V3 (..))
import qualified Waterfall
import RelativizeSocialUrls (relativizeSocialUrls)
import SiteRoot (siteRoot)
import BaseContext (baseContext)
import Text.Pandoc.Builder (image)
import Data.Maybe (catMaybes, mapMaybe)
import Data.Char (isSpace)
import BuildPhotoLinks (makeSimplePhotoLinkHTML)
import Control.Arrow (left)
import Data.Function (on)


loadGlbFiles :: FilePath -> Compiler [String]
loadGlbFiles path = itemBody <$> loadSnapshot (setVersion (Just "rich") . fromFilePath  $ path) "waterfallGltfFiles"

codeBlock :: Item String -> Item P.Pandoc
codeBlock = fmap (P.Pandoc mempty . pure . P.CodeBlock ("", ["haskell"], []) . T.pack)

codeTag :: String -> String 
codeTag txt = "<code>" <> txt <> "</code>"

commentPrefix :: String -> String 
commentPrefix s = "-- " <> s <> ":" 

parseCommentField :: String -> String -> Maybe String
parseCommentField identifier s | prefix `isPrefixOf` s = Just .dropWhile isSpace . drop (length prefix) $ s
                               | otherwise = Nothing
    where prefix = commentPrefix identifier

parseAllCommentFields :: String -> String -> [String]
parseAllCommentFields identifier = mapMaybe (parseCommentField identifier) . lines

imageCommentIdentifier :: String
imageCommentIdentifier = "image"

descriptionCommentIdentifier :: String
descriptionCommentIdentifier = "description"

shortDescriptionCommentIdentifier :: String
shortDescriptionCommentIdentifier = "short-description"

parseAllImageComments :: String -> [String]
parseAllImageComments = parseAllCommentFields imageCommentIdentifier

prefixifyImageUrls :: String -> String
prefixifyImageUrls = 
    let doOne s = 
          case parseCommentField imageCommentIdentifier s of 
            Just url -> commentPrefix imageCommentIdentifier <> " " <> siteRoot <> url
            Nothing -> s
    in unlines . fmap doOne . lines

descriptionCommentPrefix :: String 
descriptionCommentPrefix = "-- description"

descriptionOrShortDescFallback :: String -> String
descriptionOrShortDescFallback t =
    case parseAllCommentFields descriptionCommentIdentifier t of 
        [] -> shortDescription t
        desc -> intercalate "\n" desc


htmlDescription :: String -> String
htmlDescription = either (T.unpack . P.renderError) T.unpack 
    .  P.runPure
    . (P.writeHtml5String defaultHakyllWriterOptions 
        <=< P.readMarkdown defaultHakyllReaderOptions) 
    . T.pack 
    . descriptionOrShortDescFallback

shortDescription :: String -> String
shortDescription = intercalate "\n" . parseAllCommentFields shortDescriptionCommentIdentifier

newtype WaterfallResult = WaterfallResult (String, [CopyFile])
    deriving Typeable
    deriving (Binary) via (String, [CopyFile]) 

instance Writable WaterfallResult where 
    write :: FilePath -> Item WaterfallResult -> IO ()
    write path (Item ident (WaterfallResult (contents, stls))) = do
        write path (Item ident contents)
        print ("hello", path)
        forM_ stls $ \(CopyFile tmpPath) -> do
            let dir = replaceExtensions path "files"
            createDirectoryIfMissing True dir
            let filename = takeFileName tmpPath
            let dst = dir </> filename
            print (path, dst)
            write dst (Item (setVersion (Just filename) ident) (CopyFile tmpPath))

makeGltfVersion :: FilePath -> IO ()
makeGltfVersion filepath = do
    s <- Waterfall.readSolid filepath
    let newFilePath = replaceExtensions filepath "glb"
    Waterfall.writeGLB 0.01 newFilePath $ Waterfall.rotate (V3 1 0 0) (-pi/2) s 
    print newFilePath

waterfallRoutes :: Rules ()
waterfallRoutes = do 
    match "waterfall/*" $ version "raw" $ do
        route   idRoute
        compile $ do
            getResourceBody <&> fmap prefixifyImageUrls

    match "waterfall/*" $ version "rich" $ do
        route   $ customRoute $ (<> ".html") . toFilePath
        let snippetCtx =
                pathField "rawpath" <>
                mapContext (takeFileName) (pathField "title") <>
                mapContext (codeTag . takeFileName) (pathField "titleHTML") <>
                baseContext
        compile $ do
            TmpFile tmpfilepath <- newTmpFile ""
            unsafeCompiler $ createDirectoryIfMissing True tmpfilepath
            cwd <- unsafeCompiler $ getCurrentDirectory
            filepath <- getResourceFilePath
            contents' <- itemBody <$> getResourceBody
            let contents = T.pack contents' 
            let images = parseAllImageComments contents'
            let htmlDesc = htmlDescription contents'
            let shortDesc = shortDescription contents'
            (stlFiles, files) <- unsafeCompiler $ do
                readProcess "stack" ["runhaskell", "--cwd", tmpfilepath, cwd </> filepath] ""
                let indexOf f = T.length . fst $ T.breakOn (T.pack f) contents
                stlFiles <- fmap (tmpfilepath </>) . sortOn indexOf <$> getRecursiveContents (pure . (/= ".stl") . takeExtension) tmpfilepath
                print stlFiles 
                traverse_ makeGltfVersion stlFiles
                allFiles <- fmap (tmpfilepath </>) <$> getRecursiveContents (pure . const False) tmpfilepath
                return (stlFiles, allFiles)
            let stlFileFor stlFile = replaceExtensions filepath "files" </> takeFileName stlFile
            let gltfFileFor stlFile = replaceExtensions stlFile "glb"
            let snippetCtx =
                        listFieldWith "models" 
                            ( field "glb" (pure . gltfFileFor . itemBody) <>
                              field "stlFilename" (pure . takeFileName . itemBody) <>
                              bodyField "stl"
                            ) (\i -> pure $ (<$ i) . stlFileFor <$> stlFiles) <>
                        mapContext ((siteRoot </>) . (<> ".oembed.json")) (pathField "oembed_url") <>
                        constField "social_image_url"  (siteRoot </> "images/social/waterfall-cad.png") <>
                        constField "description" shortDesc <>
                        constField "rich_description" htmlDesc <>
                        pathField "rawpath" <>
                        mapContext (takeFileName) (pathField "title") <>
                        mapContext (codeTag . takeFileName) (pathField "titleHTML") <>
                        listField "images" (bodyField "image") (traverse (makeItem . makeSimplePhotoLinkHTML) images) <>
                        baseContext
            _ <- saveSnapshot "waterfallFiles" . (takeFileName filepath <$) =<< getResourceBody
            snap <- saveSnapshot "waterfallGltfFiles" . ((gltfFileFor . stlFileFor <$> stlFiles) <$) =<< getResourceBody
            unsafeCompiler $ print snap
            contents <- getResourceBody 
                <&> fmap prefixifyImageUrls
                >>= saveSnapshot "snippet"
                <&> (writePandoc . codeBlock) 
                >>= loadAndApplyTemplate "templates/waterfall.html" snippetCtx 
                >>= loadAndApplyTemplate "templates/default.html" snippetCtx 
                >>= relativizeUrls 
            return $ (\c -> WaterfallResult (c, CopyFile <$> files)) <$> contents

    match "waterfall/*" $ version "oembed_json" $ do
        route   $ customRoute $ (<> ".oembed.json") . toFilePath
        compile $ do
            let ctx = mapContext takeFileName (pathField "title")
                        <> constField "social_image_url"  (siteRoot </> "images/social/waterfall-cad.png")
                        <> constField "oembed_thumbnail"  (siteRoot </> "images/social/waterfall-cad-oembed-preview.png")
                        <> mapContext ((siteRoot </>) . (<> ".oembed.html")) (pathField "oembed_html")
                        <> baseContext
            makeItem "" 
               >>= loadAndApplyTemplate "templates/oembed.json" ctx

    match "waterfall/*" $ version "oembed_html" $ do
        route   $ customRoute $ (<> ".oembed.html") . toFilePath
        compile $ do
            filepath <- getResourceFilePath
            glbFiles <- loadGlbFiles filepath
            let ctx = mapContext takeFileName (pathField "title")
                        <> constField "glb_url" (head glbFiles) 
                        <> baseContext
            makeItem "" 
               >>= loadAndApplyTemplate "templates/oembed.html" ctx
               >>= relativizeUrls 

    create ["waterfall/index.html"] $ version "waterfall-index" $ do
        route idRoute
        compile $ do
            posts <- loadAllSnapshots ("waterfall/*" .&&. hasVersion "rich") "waterfallFiles"
            unsafeCompiler $ print posts
            let indexCtx =
                    listField "posts" (bodyField "title" <> baseContext) (return posts) <>
                    constField "title" "Waterfall CAD - 3D Models" <>
                    baseContext

            makeItem "" 
                >>= loadAndApplyTemplate "templates/waterfall-index.html" indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls

