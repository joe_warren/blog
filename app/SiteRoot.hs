{-# LANGUAGE OverloadedStrings #-}
module SiteRoot (siteRoot) where

import Data.String (IsString)

siteRoot :: IsString t => t
--siteRoot = "https://doscienceto.it/blog-staging"
siteRoot = "https://doscienceto.it/blog"
--siteRoot = "http://localhost:8000"
