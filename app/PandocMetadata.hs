{-# LANGUAGE OverloadedStrings #-}
module PandocMetadata
(metaToContext
,emptyMetadata
,Metadata (..)
) where

import           Data.Text (Text)
import qualified Data.Text as T
import           Hakyll.Core.Compiler
import           Hakyll.Core.Item
import           Hakyll.Web.Template.Context (Context, field)
import           Text.Pandoc
import qualified GHC.Generics as G
import Data.Aeson (encode, decode)
import qualified Data.Aeson as A
import qualified Data.Binary as Bin
import qualified Data.Binary.Put as Bin
import qualified Data.Binary.Get as Bin
import qualified Data.Map               as M

inlinesToString :: [Inline] -> Text
inlinesToString inlines = foldMap inlineToString inlines
  where
    inlineToString (Str a) = a
    inlineToString (Space) = " "

extractMeta :: Text -> MetaValue -> Context a
extractMeta name metavalue =
    case metavalue of
      MetaInlines inlines -> mkField . T.unpack $ inlinesToString inlines
      _ -> mempty
    where
      mkField = field (T.unpack name) . const . return


newtype Metadata = Metadata {getMetadata :: Meta} 
emptyMetadata :: Metadata
emptyMetadata = Metadata  mempty

--instance G.Generic Metadata
instance Bin.Binary Metadata where
    put (Metadata meta) = Bin.put $ encode meta
    get = do
        bs <- Bin.get
        case decode bs of
            Just r -> return $ Metadata r 
            Nothing -> fail "bad metadata json parse"
        

metaToContext :: Metadata -> Context a
metaToContext meta =  M.foldMapWithKey extractMeta (unMeta . getMetadata $ meta)

