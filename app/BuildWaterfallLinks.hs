{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE QuasiQuotes #-}
module BuildWaterfallLinks 
(buildWaterfallLinks
) where
-----------------------------
import qualified Data.Text              as T
import           Data.Text              (Text (..))
import           Data.List
import           Data.Maybe             (fromMaybe)
import           Text.Pandoc
import           Text.Pandoc.Walk
import           Hakyll.Web.Pandoc      (writePandoc, defaultHakyllReaderOptions)
import Hakyll (Compiler, loadSnapshot, itemBody, fromFilePath, unsafeCompiler, setVersion)
import System.FilePath (takeFileName)
import Data.String.Interpolate ( i )

import WaterfallRoutes (loadGlbFiles)

writeAttr :: Attr -> Text
writeAttr (identifier, classes, kv) = 
    let identifier' = 
            if identifier == ""
                then ""
                else "id=\"" <> identifier <> "\"\n"
        classes' = 
            case classes of
                    [] -> ""
                    _ -> "class=\"" <> T.intercalate " " classes <> "\"\n"
        kv'' (k, v) = k <> "=\"" <> v <> "\"\n"
        kv' = T.concat $ kv'' <$> kv
     in identifier' <> classes' <> kv'

makeWaterfallLink :: Attr -> [Inline] -> Text -> Text -> Compiler Inline
makeWaterfallLink meta inline url title = do
     gltfFiles <- loadGlbFiles (T.unpack url)
     unsafeCompiler $ print gltfFiles 
     let (src :: String) = head gltfFiles
     let filename = takeFileName . T.unpack $ url
     pure $ RawInline (Format "HTML") [i|
<model-viewer 
     #{writeAttr meta}
     src=../#{src}
     environment-image="../images/hdri/aircraft_workshop_01_1k.hdr"
     ar 
     shadow-intensity="1" 
     camera-controls 
     touch-action="pan-y" 
     auto-rotate 
     interaction-prompt="none"
     rotation-per-second="90deg"
     >
     <a class="model-link" href="../#{url}.html">#{filename}</a>
</model-viewer>
|]

transformInline :: Inline -> Compiler Inline
transformInline (img@(Image meta contents (url, title))) | T.isPrefixOf "waterfall/" url = makeWaterfallLink meta contents url title
                                             | otherwise = pure img
transformInline other = pure other

buildWaterfallLinks :: Pandoc -> Compiler Pandoc
buildWaterfallLinks doc = walkM transformInline doc
