{-# LANGUAGE OverloadedStrings #-}
module BuildHeaderLinks 
(buildHeaderLinks
) where
-----------------------------
import Text.Pandoc ( Pandoc, Inline(Link), Block(Header) )
import Text.Pandoc.Walk (walk)
import Text.Pandoc.Shared (stringify)

transformHeader :: Block -> Block
transformHeader header@(Header level attr@(identifier, _, _) contents) | level > 1 = 
    let linkClass = "headerLink"
        linkAttr = ("", [linkClass], [])
        linkDestination = "#" <> identifier
        linkTitle = stringify contents
    in Header level attr 
            [ Link linkAttr contents (linkDestination, linkTitle)
            ]
transformHeader block = block 

buildHeaderLinks :: Pandoc -> Pandoc
buildHeaderLinks = walk transformHeader
