{-# LANGUAGE OverloadedStrings #-}
module EnrichLinks
( getLinkCounts
, enrichLinks) where

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Map as M
import Data.Map (Map)
import Text.Pandoc.Walk (walk, query)
import Text.Pandoc (Inline(Link, Span, Image), Pandoc)
import Data.Monoid (Sum (..))
import Data.Map.Monoidal (MonoidalMap(..))
import Data.Coerce (coerce)
import Network.URI (parseURIReference, uriAuthority, uriRegName)
import Control.Arrow ((&&&))

getDomain :: Text -> Text
getDomain = T.pack . maybe "???" (maybe "self" uriRegName . uriAuthority) . parseURIReference . T.unpack

replaceDots :: Text -> Text
replaceDots = T.replace "." "-"

getLinkCounts :: Pandoc -> MonoidalMap (Text, Text) (Sum Integer)
getLinkCounts =
    let getOneLinkCount (Link _ _ (target, _)) = MonoidalMap $ M.singleton ( (id &&& replaceDots). getDomain $ target) (Sum (1 :: Integer))
        getOneLinkCount _ = mempty
    in query getOneLinkCount

enrichLinks :: Pandoc -> Pandoc
enrichLinks =
    let enrichOne imageLink@(Link _ [Image _ _ _] _) = imageLink -- if a link only contains an image, don't include it
        enrichOne (Link (identifier, classes, props) inlines tt@(target, _)) =
            let 
                newClasses = ["link-preview", "link-to-" <> (replaceDots . getDomain $ target)]
            in Link (identifier, classes <> newClasses, props) inlines tt
        enrichOne inline = inline
    in walk enrichOne

