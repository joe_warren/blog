{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
import           Data.Monoid (mappend, Sum (getSum), Any)
import           Data.Text (Text)
import qualified Data.Text as T
import           Hakyll
import Hakyll.Core.Compiler.Internal (compilerTellDependencies)
import Hakyll.Images        ( loadImage
                            , compressJpgCompiler
                            , scaleImageCompiler
                            )

import           Text.Blaze.Html                 (toHtml, toValue, (!))
import           Text.Blaze.Html.Renderer.String (renderHtml)
import qualified Text.Blaze.Html5                as H
import qualified Text.Blaze.Html5.Attributes     as A

import WaterfallRoutes (waterfallRoutes)
import RelativizeSocialUrls (relativizeSocialUrls)
import BuildPhotoLinks (buildPhotoLinks)
import BuildHeaderLinks (buildHeaderLinks)
import BuildWaterfallLinks (buildWaterfallLinks)
import EnrichLinks (getLinkCounts, enrichLinks)
import SiteRoot (siteRoot)
import BaseContext (baseContext)

import qualified  LiterateCompiler as LC
import qualified PandocMetadata as PM
import qualified Text.Pandoc as P

import Control.Applicative ((<|>))
import Control.Monad (void)
import Data.Function (on)
import Data.Foldable (toList)
import Data.List (isSuffixOf, intersperse)
import Data.Char (toLower)
import qualified Data.Map as M
import qualified Data.Yaml as Yaml
import qualified Data.ByteString.Lazy as BSL
import Text.Pandoc (Block(CodeBlock))
import Data.Functor ((<&>))
import System.FilePath (takeFileName, (</>))
import Data.Map.Monoidal (MonoidalMap(..))
import Data.Map (Map)
import Control.Arrow (second)
import Data.Time.Format (defaultTimeLocale)
import Data.Time.Clock (utctDay)
import qualified Stats 
--------------------------------------------------------------------------------

(<$$>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<$$>) f = fmap (fmap f)


myFeedConfiguration :: FeedConfiguration
myFeedConfiguration = FeedConfiguration
    { feedTitle       = "Joe's Blog"
    , feedDescription = "A very intermittent personal blog"
    , feedAuthorName  = "Joe Warren"
    , feedAuthorEmail = "dev/null"
    , feedRoot        = siteRoot
    }

regularPandocCompiler :: Compiler (Item P.Pandoc)
regularPandocCompiler = readPandoc =<< getResourceBody

literateOrRegularCompiler :: Compiler (Item P.Pandoc)
literateOrRegularCompiler = LC.literatePandocCompiler <|> regularPandocCompiler

codeBlock :: Item String -> Item P.Pandoc
codeBlock = fmap (P.Pandoc mempty . pure . P.CodeBlock ("", ["haskell"], []) . T.pack)

codeTag :: String -> String 
codeTag txt = "<code>" <> txt <> "</code>"

postPlusMetaCompiler :: Compiler (Item String, PM.Metadata)
postPlusMetaCompiler = do
    pandoc <- fmap (buildHeaderLinks . buildPhotoLinks . enrichLinks) <$> (traverse buildWaterfallLinks =<< literateOrRegularCompiler)
    let linkCounts = getMonoidalMap . getLinkCounts <$> pandoc
    saveSnapshot "link-counts" linkCounts
    let output = writePandoc pandoc
    let (Item _ (P.Pandoc meta _)) = pandoc
    return (output, PM.Metadata meta)

itemSpecificContext :: Identifier -> Context a -> Context a
itemSpecificContext itemi (Context ctx) =
      Context (\a b (i2@(Item itemi2 _)) -> if itemi2 == itemi then ctx a b i2 else (unContext missingField ) a b i2)

flattenContexts :: [Item (String, PM.Metadata)] -> ([Item String], Context String)
flattenContexts l = (items, context)
  where 
    items = fmap (fmap fst) $ l
    contexts = fmap (fmap (PM.metaToContext.snd)) $ l
    itemContext (Item itemi ctx) = itemSpecificContext itemi ctx
    context = mconcat $ itemContext <$> contexts

findLeft :: (a -> Bool) -> [a] -> Maybe a
findLeft f (x1:x2:xs) | f x2 = Just x1 
                      | otherwise = findLeft f (x2:xs)
findLeft f _ = Nothing

findRight :: (a -> Bool) -> [a] -> Maybe a
findRight f (x1:x2:xs) | f x1 = Just x2
                       | otherwise = findRight f (x2:xs)
findRight f _ = Nothing

neighbouringContexts :: Item String -> [Item String] -> Context String -> Context String
neighbouringContexts i is ctx = prevContext <> nextContext
  where
    equalsi = ((==) `on` itemIdentifier) i
    prev = findLeft equalsi is
    prevContext = listField "previous" (ctx) (return $ toList prev) 
    next = findRight equalsi is
    nextContext = listField "next" (ctx) (return $ toList next) 

getTags' :: MonadMetadata m => Identifier -> m [String]
getTags' identifier = do 
    if (isSuffixOf ".scala") . toFilePath $ identifier
        then return ["Scala", "Programming", "Literate"]
        else getTags identifier

main :: IO ()
main = hakyll $ do

    match "tags.yaml" $ do
        route idRoute
        compile $ 
            (() <$) <$> (getResourceLBS >>= saveSnapshot "yaml")

    match "images/**" $ do
        route   idRoute
        compile copyFileCompiler

    match "snippets/*" $ version "raw" $ do
        route   idRoute
        compile copyFileCompiler
    
    match "snippets/*" $ version "rich" $ do
        route   $ customRoute $ (<> ".html") . toFilePath
        let snippetCtx =
                pathField "rawpath" <>
                mapContext (codeTag . takeFileName) (pathField "titleHTML") <>
                mapContext (takeFileName) (pathField "title") <>
                baseContext
        compile $ do
            getResourceBody 
                >>= saveSnapshot "snippet"
                <&> (writePandoc . codeBlock) 
                >>= loadAndApplyTemplate "templates/snippet.html" snippetCtx 
                >>= loadAndApplyTemplate "templates/default.html" snippetCtx 
                >>= relativizeUrls 
    
    waterfallRoutes

    match "photos/*" $ version "raw" $ do
        route   idRoute
        compile copyFileCompiler

    match "photos/*" $ do
        route   $ setExtension ".thumb.jpg"
        compile $ loadImage
            >>= scaleImageCompiler 800 800
            >>= compressJpgCompiler 95

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match (fromList ["about.markdown"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" baseContext
            >>= relativizeUrls
            >>= relativizeSocialUrls

    let postsPattern = ("posts/*" .&&. hasNoVersion)
    let loadPattern pat = flattenContexts <$> loadAllSnapshots pat "content-context"
    let loadPosts = loadPattern postsPattern

    let recipesPattern = ("recipes/*" .&&. hasNoVersion)
    let loadRecipePattern pat = flattenContexts <$> loadAllSnapshots pat "recipe-content-context"
    let loadRecipes = loadRecipePattern recipesPattern

    tags <- buildTagsWith getTags' "posts/*" (fromCapture "tags/*.html" . fmap toLower)

    match "posts/*.scala" $ version "raw-source" $ do
        route idRoute
        compile copyFileCompiler

    match "posts/*" $ do -- $ version "posts" $ do
        route $ setExtension "html"
        compile $ do
                    (content, meta) <- postPlusMetaCompiler
                    let ctx = PM.metaToContext meta
                    tagDescription <- readTagDescriptionFile'
                    let postCtx' =
                            mapContextBy (== "social_image_url") (siteRoot </>) $
                                ctx <> postCtxWithTags tagDescription tags
                    html <- loadAndApplyTemplate "templates/post.html" postCtx' content
                    saveSnapshot "content-context" $ 
                        itemSetBody (itemBody html, meta) html
                    (allPosts, allCtx) <- loadPosts
                    let neighbourCtx = neighbouringContexts html allPosts (allCtx <>postCtx) 
                    saveSnapshot "content" html
                        >>= loadAndApplyTemplate "templates/prev-next.html" (neighbourCtx <> postCtx')
                        >>= loadAndApplyTemplate "templates/default.html" postCtx'
                        >>= relativizeUrls
                        >>= relativizeSocialUrls

    match "recipes/*" $ do 
        route $ setExtension "html"
        compile $ do
                    (content, meta) <- postPlusMetaCompiler
                    let ctx = PM.metaToContext meta
                    tagDescription <- readTagDescriptionFile'
                    let postCtx' = ctx <> recipeCtx
                    html <- loadAndApplyTemplate "templates/recipe.html" postCtx' content
                    saveSnapshot "recipe-content-context" $ 
                        itemSetBody (itemBody html, meta) html
                    (allPosts, allCtx) <- loadRecipes
                    let neighbourCtx = neighbouringContexts html allPosts (allCtx <>postCtx) 
                    saveSnapshot "recipe-content" html
                        >>= loadAndApplyTemplate "templates/prev-next.html" (neighbourCtx <> postCtx')
                        >>= loadAndApplyTemplate "templates/default.html" postCtx'
                        >>= relativizeUrls
                        >>= relativizeSocialUrls

    create ["recipes/index.html"] $ version "recipe-index" $ do
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadRecipes
            --posts <- recentFirst allPosts
            let indexCtx =
                    listField "posts" (ctx<>recipeCtx) (return allPosts) `mappend`
                    constField "title" "Family Cookbook"            `mappend`
                    recipeCtx

            makeItem "" 
                >>= loadAndApplyTemplate "templates/recipes-index.html" indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadPosts
            posts <- recentFirst allPosts
            let archiveCtx =
                    listField "posts" (ctx <>postCtx) (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    baseContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls

                
    create ["links.html"] $ do
        route idRoute
        compile $ do
            counts :: [Item (Map (Text, Text) (Sum Integer))] <- loadAllSnapshots postsPattern "link-counts"
            let mergedCounts = fmap (second getSum). M.toList . getMonoidalMap $  foldMap (MonoidalMap . itemBody) counts  
            let countContext = 
                    field "link" (return . T.unpack . fst . fst . itemBody) <>
                    field "escaped-link" (return . T.unpack . snd . fst . itemBody) <>
                    field "count" (return . show . snd . itemBody) 
            let pageCtx =
                    listField "link-counts" countContext (traverse makeItem mergedCounts) `mappend`
                    constField "title" "Links"            `mappend`
                    baseContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/links.html" pageCtx
                >>= loadAndApplyTemplate "templates/default.html" pageCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls

    create ["years.svg"] $ do
        route idRoute  
        compile $ do
            (allPosts, ctx) <- loadPosts
            traverse (getItemUTC defaultTimeLocale . itemIdentifier) allPosts 
                >>= Stats.renderYearsChart "Posts by Year"
                
    create ["months.svg"] $ do
        route idRoute  
        compile $ do
            (allPosts, ctx) <- loadPosts
            traverse (getItemUTC defaultTimeLocale . itemIdentifier) allPosts
                >>= Stats.renderMonthsChart "Posts by Month"
                
    create ["tags.svg"] $ do
        route idRoute  
        compile $ do
            compilerTellDependencies [tagsDependency tags]
            Stats.renderTagsChart "Posts by Tag" (tagsMap tags)

    create ["stats.html"] $ do
        route idRoute
        compile $ do
            let indexCtx =
                    constField "title" "Statistics" `mappend`
                    baseContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/statistics.html" indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls

    create ["social-cards.html"] $ do
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadPosts
            posts <- recentFirst allPosts
            let indexCtx =
                    listField "posts" (ctx<>postCtx) (return posts) `mappend`
                    constField "title" "Social Cards" `mappend`
                    baseContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/social-cards.html" indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls

    tagsRules tags $ \tag pat -> do
        let title = "Tagged: " ++ tag
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadPattern pat
            posts <- recentFirst allPosts
            tagDescription <- readTagDescriptionFile'
            let ctxWithTags = postCtxWithTags tagDescription tags
            let tagCtx = constField "title" title 
                      `mappend` constField "atom_url" ("/tags/" <> (toLower <$> tag) <> ".atom.xml")
                      `mappend` constField "rss_url" ("/tags/" <> (toLower <$> tag) <> ".rss")
                      `mappend` constField "description" (tagDescription tag)
                      `mappend` listField "posts" (ctx <> ctxWithTags) (return posts)
                      `mappend` baseContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/tag.html" tagCtx
                >>= loadAndApplyTemplate "templates/default.html" tagCtx
                >>= relativizeUrls

    let compileTagFeeds feedType feedExt feedCompiler =
            tagsRules tags $ \tag pat -> version feedType $ do
                route (setExtension feedExt)
                compile $ do
                    (allPosts, ctx) <- loadPattern pat
                    posts <- recentFirst allPosts
                    let feedCtx = ctx `mappend` postCtx
                    tagDescription <- readTagDescriptionFile'
                    let feedConfiguration = myFeedConfiguration 
                            { feedTitle = feedTitle myFeedConfiguration <> " :: Only " <> tag <> " Posts"
                            , feedDescription = tagDescription tag
                            }
                    feedCompiler feedConfiguration feedCtx  posts
    compileTagFeeds "atom" "atom.xml" renderAtom
    compileTagFeeds "rss" "rss" renderRss

    create ["tags/index.html"] $ do
        route idRoute
        compile $ do
            tagsContent <- renderTagPageLinks tags
            
            let tagCtx = constField "title" "Tags" 
                      `mappend` baseContext

            makeItem tagsContent
                >>= loadAndApplyTemplate "templates/default.html" tagCtx
                >>= relativizeUrls


    match "index.html" $ do
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadPosts
            posts <- recentFirst allPosts
            let indexCtx =
                    listField "posts" (ctx<>postCtx) (return posts) `mappend`
                    baseContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls
                >>= relativizeSocialUrls


    create ["atom.xml"] $ do
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadPosts
            let feedCtx = ctx `mappend` postCtx
            posts <- recentFirst allPosts
            renderAtom myFeedConfiguration feedCtx  posts

    create ["feed.rss"] $ do
        route idRoute
        compile $ do
            (allPosts, ctx) <- loadPosts
            let feedCtx = ctx <> postCtx 

            posts <- recentFirst allPosts
                >>= traverse relativizeUrls
            renderRss myFeedConfiguration feedCtx posts

    match "templates/*" $ compile templateBodyCompiler

renderTagPageLink :: (String -> String) -> String -> Maybe FilePath -> H.Html
renderTagPageLink _    _    Nothing = ""
renderTagPageLink desc tag (Just path) =
  H.li ( 
      (H.a $ toHtml tag) 
        ! A.href (toValue $ toUrl path) 
        ! A.title (toValue $ desc tag) <>
      (toHtml $ " - " <> desc tag)
  )

renderTagPageLinks :: Tags -> Compiler String
renderTagPageLinks tags = do
    desc <- readTagDescriptionFile'
    let tags' = fst <$> tagsMap tags
    let makeId = tagsMakeId tags
    -- tagPages <- traverse (getRoute . makeId) tags'
    let renderSingle t = renderTagPageLink desc t <$> (getRoute . makeId  $ t)
    items <- traverse renderSingle tags'
    return $ renderHtml $ H.ul $ mconcat items


renderTagLink :: (String -> String) -> String -> (Maybe FilePath) -> Maybe H.Html
renderTagLink _ _   Nothing         = Nothing
renderTagLink description tag (Just filePath) =
  Just $ (H.a $ toHtml tag) 
        ! A.href (toValue $ toUrl filePath) 
        ! A.title (toValue $ description tag)
        ! A.class_ "tag"


readTagDescriptionFile :: Identifier -> String -> Compiler (String -> String)
readTagDescriptionFile filename fallback = do
    body <- loadSnapshotBody filename "yaml"
    tagDescriptions <- case Yaml.decodeEither' $ BSL.toStrict body of
        Left e -> fail $ show e
        Right r -> return r 
    return $ \s -> M.findWithDefault fallback s tagDescriptions 


readTagDescriptionFile' = readTagDescriptionFile "tags.yaml" ""

recipeCtx :: Context String 
recipeCtx = 
    constField "social_image_url" (siteRoot <> "/images/social/recipes.png") <>
    constField "description" "Page from a Family Cookbook" <>
    baseContext

postCtxWithTags :: (String -> String) -> Tags -> Context String
postCtxWithTags descriptionLookup tags = tagsFieldWith getTags' (renderTagLink descriptionLookup) (mconcat) "tagged" tags `mappend` postCtx

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    baseContext
