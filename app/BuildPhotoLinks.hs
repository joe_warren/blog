{-# LANGUAGE OverloadedStrings #-}
module BuildPhotoLinks 
( buildPhotoLinks
, makeSimplePhotoLink
, makeSimplePhotoLinkHTML
) where
-----------------------------
import qualified Data.Text              as T
import           Data.Text              (Text (..))
import           Data.List
import           Data.Maybe             (fromMaybe)
import           Text.Pandoc
import           Text.Pandoc.Walk
import           Hakyll.Web.Pandoc      (writePandoc, defaultHakyllReaderOptions)
import Data.Either (fromRight)

stripSuffix :: Text -> Text -> Maybe Text
stripSuffix suf = (fmap T.reverse) . (T.stripPrefix . T.reverse $ suf) . T.reverse

thumbnailUrl :: Text -> Text
thumbnailUrl = (<> ".thumb.jpg") . (\x -> fromMaybe x (T.stripSuffix ".jpg" x))

makePhotoLink :: Attr -> [Inline] -> Text -> Text -> Inline
makePhotoLink meta inline url title = Link meta [Image meta inline (thumbnailUrl url, title)] (url, title) 

transformInline :: Inline -> Inline
transformInline (img@(Image meta contents (url, title))) | T.isPrefixOf "/photos/" url = makePhotoLink meta contents url title
                                             | otherwise = img
transformInline other = other

buildPhotoLinks :: Pandoc -> Pandoc
buildPhotoLinks doc = walk transformInline doc

makeSimplePhotoLink :: String -> Pandoc 
makeSimplePhotoLink url = Pandoc mempty . pure . Plain . pure . transformInline $ Image nullAttr [] (T.pack url, "")

makeSimplePhotoLinkHTML :: String -> String 
makeSimplePhotoLinkHTML = T.unpack . fromRight "<error>" . runPure . writeHtml5String def . makeSimplePhotoLink 
