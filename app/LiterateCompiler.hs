{-# LANGUAGE OverloadedStrings #-}
module LiterateCompiler 
(literateCompiler
,literatePandocCompiler
) where
-----------------------------
import qualified Data.Text              as T
import           Data.Maybe             (isJust)
import           Data.List              (dropWhile, dropWhileEnd)
import           Text.Pandoc hiding (trace)
import qualified Text.Pandoc.Builder    as B
import           Text.Pandoc.Options    (writerSyntaxMap)
import           Hakyll.Core.Compiler
import           Hakyll.Core.Item
import           Hakyll.Web.Pandoc      (writePandoc, defaultHakyllReaderOptions)
import           Hakyll.Web.Template.Context (Context, field)
import           Skylighting            (defaultSyntaxMap)
import           Skylighting.Tokenizer  (tokenize, TokenizerConfig (..))
import qualified Skylighting.Types      as S
import qualified Skylighting            as S
import Debug.Trace (trace)
data LiteratePart = Source T.Text | Comment T.Text deriving (Eq, Show)

cleanComment :: T.Text -> T.Text
cleanComment text = T.strip $ T.dropWhile (== '/') $ T.dropWhile (=='*') $ T.dropWhile (/= '*') text

literifyToken :: S.Token -> LiteratePart
literifyToken (S.CommentTok, text) | isJust $ T.find (=='*') text = Comment $ cleanComment text
                                   | otherwise = Source text
literifyToken (_, text) = Source  text

smooshAlerts :: [S.Token] -> [S.Token]
smooshAlerts ((S.CommentTok, t1):(S.AlertTok, t2):(S.CommentTok, t3):tail) = (S.CommentTok, (t1<> t2<> t3)) : smooshAlerts tail
smooshAlerts (tok:tail)= tok:smooshAlerts tail
smooshAlerts [] = []

addTrailingNewlines :: [LiteratePart] -> [LiteratePart]
addTrailingNewlines [last@(Source _)] = [last, Source "\n"]
addTrailingNewlines [last@(Comment _)] = [last, Comment "\n"]
addTrailingNewlines (x:xs) = x:(addTrailingNewlines xs)
addTrailingNewlines [] = []

groupLiterateParts :: [LiteratePart] -> [LiteratePart]
groupLiterateParts ((Source t1):(Source t2):xs) = groupLiterateParts $ (Source (t1<>t2)):xs
groupLiterateParts ((Comment t1):(Comment t2):xs) = groupLiterateParts $ (Comment (t1<>t2)):xs
groupLiterateParts ((t1@(Source _)):(t2@(Comment _)):xs) = t1:(groupLiterateParts $ t2:xs)
groupLiterateParts ((t1@(Comment _)):(t2@(Source _)):xs) = t1:(groupLiterateParts $ t2:xs)
groupLiterateParts [l] = [l]
groupLiterateParts [] = []

handleOneToken :: PandocMonad m => LiteratePart -> m Pandoc
handleOneToken (Comment text) = readMarkdown defaultHakyllReaderOptions text
handleOneToken (Source text) = return $ Pandoc nullMeta [B.CodeBlock ("",["scala"],[]) text]

isAllWhitespace :: T.Text -> Bool
isAllWhitespace = T.null . T.strip

isEmptyPart :: LiteratePart -> Bool
isEmptyPart (Comment text) = isAllWhitespace $ text
isEmptyPart (Source text) = isAllWhitespace $ text

cleanBlankSource :: LiteratePart -> LiteratePart
cleanBlankSource (Comment text) = Comment text
cleanBlankSource (Source text) = Source $ T.unlines $ dropWhileEnd isAllWhitespace $ dropWhile isAllWhitespace $ T.lines text

filterEmpty :: [LiteratePart] -> [LiteratePart]
filterEmpty = (fmap cleanBlankSource).(filter $ not.isEmptyPart)

handleTokens :: PandocMonad m => [S.SourceLine] -> m [Pandoc]
handleTokens tokens = traverse handleOneToken simpleTokens 
  where 
    simpleTokens = filterEmpty $ groupLiterateParts $ concat $ addTrailingNewlines <$> fmap (fmap literifyToken . smooshAlerts) tokens

literatePandocCompiler :: Compiler (Item Pandoc)  -- ^ Resulting document
literatePandocCompiler = do
    extension <- getUnderlyingExtension
    _ <- case extension of
        ".scala" -> return ()
        _ -> fail "we only support writing scala"
    Item itemi content <- getResourceString
    --let blocks = B.codeBlockWith ("",["scala"],[]) content
    let syntaxMap = defaultSyntaxMap
    let config = TokenizerConfig syntaxMap False
    let Just syntax = "scala" `S.lookupSyntax` syntaxMap
    let Right tokens = tokenize config syntax (T.pack content)
    let maybePandoc = runPure $ mconcat <$> handleTokens tokens
    --_ <- unsafeCompiler $ print tokens
    case maybePandoc of
        Left err    -> fail $ "readPandocWith: parse failed: " ++ show err
        Right item' -> return $ Item itemi item'


literateCompiler :: Compiler (Item String)  -- ^ Resulting document
literateCompiler = writePandoc <$> literatePandocCompiler 
