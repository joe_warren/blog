module BaseContext
( baseContext 
) where

import Hakyll 
import SiteRoot (siteRoot)
import System.FilePath ((</>))

baseContext :: Context String
baseContext = 
    constField "atom_url" "/atom.xml" <>
    constField "rss_url" "/feed.rss" <>
    defaultContext <>
    constField "social_image_url" ( siteRoot </> "images/social/default.png")