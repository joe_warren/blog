---
title: Fanta Review
social_image_url: images/social/fanta.jpg
description: In which I buy, drink and review a multipack of exotic fanta
tags: Food
--- 
<style>
h2 {
  clear: both;
}
</style>

![](/photos/14-fanta.jpg "All the cans of Fanta, stacked up"){.right width=30% }

As a "tech worker" one of the many things that I've been missing under COVID 19 lockdown has been access to a fully stocked drinks fridge. 

To remedy this, I bought a multipack of 11 different cans of Fanta, and one can of "Pepsi Blue. 

These are my thoughts on them. 

## Peach
<div class="fanta-review" 
     style="background-color: #de3204;
     background-image: url(../images/fanta/peach.jpg)">
  6/10
</div>
A little flat (the can was badly dented), tart, but not very tart. 

Label reads "Contains No Juice".

Tastes _very intensely_ of peach, they've got that part pretty much spot on.
The colour is more or less that of Irn-Bru though, so that's not ideal.

Nice enough, but definitely not something you'd drink routinely. 

## Pepsi Blue
<div class="fanta-review left" 
     style="background-color: #29295c;
     background-image: url(../images/fanta/pepsi.jpg)">
  4/10
</div>
_Very Intensely Blue_, they can't be accused of falsely describing it.
Appearance suggests a cross between a molten Sonic the Hedgehog, and an as yet unused chemical toilet. 

Much like the peach, this is also a little flat, I suspect that these cans are quite old, so that might be an ongoing trend. 

[Wikipedia suggests](https://en.wikipedia.org/wiki/Pepsi_Blue) that this is supposed to be fruit flavoured: "like blueberries or raspberries, or similar to cotton candy with a berry-like after taste".

I'm mostly tasting Cola: I don't really ever drink Pepsi, so I wouldn't confidently state how much this _tastes_ different to regular Pepsi; it's certainly similar, possibly sweeter, but I could only imagine it being described like "blueberries and raspberries" by someone who had never tasted either. 

It's not _terrible_ but I wouldn't recommend it.

## Grapefruit (Toronja)
<div class="fanta-review" 
     style="background-color: #00a29d;
     background-image: url(../images/fanta/grapefruit.jpg)">
  8/10
</div>
This exploded, so I only got half a can.

Surprisingly flat, considering that it exploded. 

It's good though, not _overly grapefruity_, I think I would have preferred something more acidic and less sweet, although that's possibly a sign that I should be drinking something other than Fanta. 

It definitely does taste of grapefruit though, and I'm a big grapefruit fan. 

## Fruit Punch
<div class="fanta-review left" 
     style="background-color: #902e2d;
     background-image: url(../images/fanta/fruit-punch.jpg)">
  5/10
</div>
This was decently fizzy for once, and viscerally pink, like an electrified version of cotton candy. 

The main flavour is of watermelon, but there are hints of berries in there too.In lots of ways, this is similar to the peach; the can also proclaims "Contains No Juice" for one. 

The "Fruit" isn't quite as authentic as the Peach, hence the lower score.

## Pineapple
<div class="fanta-review" 
     style="background-color: #9f821a;
     background-image: url(../images/fanta/pineapple.jpg)">
  6/10
</div>
Vivid yellow, to the extent that you could almost imagine it was fluorescing. 

Slightly sour; it does taste of pineapple, but a very synthetic chemically pineapple, like a pineapple was assaulted by a gang of pear drops, and then left to drown in a vat of cream soda. 

The flavour lingers on the tongue for far longer than I expected. 
I had high hopes for this one, and it's _fine_, but not the best so far.

## Strawberry
<div class="fanta-review left" 
     style="background-color: #a00f16;
     background-image: url(../images/fanta/strawberry.jpg)">
  5/10
</div>
The can says "100% Natural Flavours" and also "High Fructose Corn Syrup", and I find the latter statement the easiest of the two to believe. 

That said, it does taste of strawberry, it's just very sweet. 
Its coloured an intense shade of red, but then so are strawberries. 

I can't say I love this, but then I think there's a limit to how much it's possible for me to love a strawberry flavoured fizzy drink. 

## Grape
<div class="fanta-review" 
     style="background-color: #170c10;
     background-image: url(../images/fanta/grape.jpg)">
  7/10
</div>
I don't know if grape soda is supposed to taste of "grape", rather than tasting of "grape soda", which is it's own unique thing. 
This almost tastes of both, there's a lingering aftertaste of something that you could reasonably compare to grape, but the dominating flavour is _something else_. 

In my opinion, that's fine, this doesn't taste horrible. 

It's not as aggressively sweet as some of the other Fantas have been, although that may be my perception because I've chilled it for longer. 

The colour is very dark; purple crossed with black. 

## Wild Cherry
<div class="fanta-review left" 
     style="background-color: #8f0610;
     background-image: url(../images/fanta/wild-cherry.jpg)">
  7/10
</div>
Very red, tastes _very much of cherry _flavouring_; reminiscent of cough drops or boiled sweets but slightly more refreshing.

I drank this on a hot day after coming back from a run, when I was super thirsty so it's possible that I enjoyed it more than it was objectively _good_. 
With that said, no real complaints. 

## Green Apple
<div class="fanta-review" 
     style="background-color: #3e8e19;
     background-image: url(../images/fanta/green-apple.jpg)">
  5/10
</div>
An unreasonable shade of green. 

There's a risk that my entire perception of "Green Apple" was coloured by my initial shock of how electrically green it is. 

Flavour wise, in my opinion, the ideal apple soda should taste like "carbonated apple juice", for instance "Tango Apple" or "Appetiser". 

This _definitely isn't that_. 

This is far stronger.

It's clear that they're aiming for apple, but this has the same chemical notes as the "Wild Cherry" and the "Pineapple"· 

## Apple
<div class="fanta-review left" 
     style="background-color: #f3f2c6;
     background-image: url(../images/fanta/apple.jpg)">
 7/10
</div>
When I said, "the ideal flavour of apple soda is carbonated apple juice" in the "Green Apple" review, this is far closer. 

It's also apple coloured, rather than bright green. 

I still don't think it get's it quite right, this would be very sweet for apple juice, and I _think_ I got notes of caramel, but it's unambiguously an improvement.

## Berry
<div class="fanta-review" 
     style="background-color: #12609e;
     background-image: url(../images/fanta/berry.jpg)">
 4/10
</div>
It's a similar colour to the Pepsi blue, possibly slightly lighter. 

The can suggests that it's supposed to taste of blueberries and strawberries, which it doesn't. 

It certainly tastes of _something_, but it's a difficult flavour to pin down any more finely than just "pop".

It's sweet, but not _overtly sweet_, sour but not _overtly sour_, it's difficult to think of anything that it is to any extent.

I drank this under similar circumstances to the wild cherry, while dehydrated from running, and while it was _refreshing_, I wouldn't say I came away with an especially positive impression of it.

## Mango
<div class="fanta-review left"  
     style="background-color: #b1700c;
     background-image: url(../images/fanta/mango.jpg)">
 7/10
</div>
In the "pineapple" review I use the word "synthetic" like a dirty word. 

Much like that, this doesn't taste exactly like mango juice, but unlike the pineapple, I think it pulls it off. 

Mango juice can often be sweet to the point of being sickly, and with an extra  hint of citric acid, this manages to be un-mango like enough to avoid that. 

Colour wise, it's slightly more fluorescent than a mango, but again, I think that's appropriate for a can of Fanta. 

