---
title: Tiny Rebel Christmas 2023
social_image_url: images/social/tiny-rebel-2023.jpg
description: In which I swear that Christmas comes later every year
tags: Food
---

Unlike [previous](./2021-12-26-tiny-rebel-advent.html) [years](./2023-01-28-tiny-rebel-advent-2022.html) Tiny Rebel didn't do an advent calendar for 2023, but they did do a Christmas selection box.

These are my thoughts.

## Juice Shack - Tropical Session IPA (3.5%)

![](/photos/TinyRebel-2023-01.jpg "Juice Shack - Tropical Session IPA"){.right height=150px}

That is _super_ fruity. I think I'm getting papaya, but I think it's just kind of generic "mango and pineapple" tropical. 

You can tell it's fairly weak, and the hoppy IPA elements, while present, are not particularly strong. 

There is a reasonable degree of bitterness to it, which hits at a different speed to the fruity flavours.
Without the bitterness, I think it'd be reasonable to ask if this were intended for someone who'd rather be drinking fruit juice than beer, but with it, I think it's fair to say that there's some degree of complexity. 

Surprisingly clear for a Tiny Rebel limited run beer; which is to say that it's only murky, and not _damn_ murky.

## Superhero Landing - Salted Caramel Popcorn Imperial Milk Stout (13%)

![](/photos/TinyRebel-2023-02.jpg "Superhero Landing - Salted Caramel Popcorn Imperial Milk Stout"){.right height=150px}

Thick, rich and chocolatey. 

This is the kind of thing that I like, but it's arguably a bit much.

Salted caramel notes are very forward, with a deep cocoa-ey follow up.

Feels _damn filling_, and like it could probably knock you on your arse[^1].

[^1]: I can since confirm that this did knock me on my arse.

## Cherry Amaretto Bomb - Cherry Amaretto Sour (4.5%)

![](/photos/TinyRebel-2023-03.jpg "Cherry Amaretto Bomb - Cherry Amaretto Sour"){.right height=150px}

The first thought I had after cracking the can was "damn that's fragrant". 

Heavy cherry scent. 

Like the cherry flavour, the almond notes are more prominent on the scent, and a good bit fainter on the tongue.

Initial thoughts on drinking were that it's relatively sharp, but not offensively so, and I wonder if it'd be nicer if it had been tarter.

There's a fair amount of bitterness from the almonds, I think maybe a little too much for the acidity.

It's also not particularly sweet, and I don't think the bitterness plays well with this either.

I really like fruity beer, and I really like sour beer, so you'd think this would be very much my bag; but unfortunately I have to file it down as "fine but not great". 

After drinking more, I actually really like the cherry and almond flavour that they've achieved, it's pleasingly complex, and when you focus in on that element, the drink really works.
It took me about half of the can to learn to focus in on the parts of it that I like.

## Pinchers of Peril - Citra, Mosaic and Simcoe Pale Ale (5%)


![](/photos/TinyRebel-2023-04.jpg "Pinchers of Peril - Citra, Mosaic and Simcoe Pale Ale"){.right height=150px}

This is nice, simultaneously hoppy and fruity, remarkably like a dialed down Neipa[^2].

Good and refreshing, and I feel like the alcohol content could sneak up on you.

Solid beer, perhaps notably the first one in the list without some kind of gimmick.

[^2]: Also it's indisputable that we're back to Tiny Rebel's "murky" one-shots. Something else it has in common with most with most Neipas.

## The Vader Shuffle - Wood Aged Belgian Porter (6.5%)

![](/photos/TinyRebel-2023-05.jpg "The Vader Shuffle - Wood Aged Belgian Porter"){.right height=150px}

Surprisingly sharp, especially for a porter, to the extent that I'm concerned that this has potentially stored badly. 

The beer is definitely nice though (whether or not the acidity is intentional), good dark chocolatey malty flavour, with a complex set of spicy notes. Pleasingly rich.

## Happy Scream - Cream IPA (4.3%)

![](/photos/TinyRebel-2023-06.jpg "Happy Scream - Cream IPA"){.right height=150px}

For an IPA, surprisingly rich and sweet.

There's still the typical sharp and hoppy, IPA flavour, but it's secondary.

Feels boozier than it is.

## Tutti Frutti Ice Blast - Tropical Ice Blast Sour (4.4%)

![](/photos/TinyRebel-2023-07.jpg "Tutti Frutti Ice Blast - Tropical Ice Blast Sour"){.right height=150px}

This _genuinely smells like kid's pop_: sherbet and blue raspberry.

I couldn't confidently tell you what specific fruit this is supposed to be.

Very tart, also slightly bitter.

Not all that sweet, which means the sensation of drinking the stuff isn't that much like drinking pop, 

There's visible sediment, which is surprising, it's also a fairly lurid red.

It's an interesting novelty, after the first few sips, I initially wrote "I'm not convinced that it's something I want to come back too", but having finished the can, it actually went down fairly well.

## Lost Art - Lager IPA (4%)

![](/photos/TinyRebel-2023-08.jpg "Lost Art - Lager IPA"){.right height=150px}

This is apparently "blending Lager character with IPA notes". 

Personally I think it's closer to lager then IPA.

Hoppy, but not _very hoppy_, maybe a little citrussy, crisp, refreshing and generally pretty damn nice.

## 505 Lemon Drop Martini - Lemon Drop Neipa (6.2%)

![](/photos/TinyRebel-2023-09.jpg "505 Lemon Drop Martini - Lemon Drop Neipa"){.right height=150px}

Full bodied and rich, complemented against sharp and citrusy.

There's undeniably a flavour of lemon drop sweets, which does work with the Neipa, but is also a little strange.

## What's Cooler than Being Cool - 8% Neipa (8%) 

![](/photos/TinyRebel-2023-10.jpg "What's Cooler than Being Cool - 8% Neipa"){.right height=150px}

Hey Ya.

Because of the relatively high alcohol content, I was expecting this to go hard on "being a Neipa". 

And while it indisputably is one, I wouldn't say it was playing it up.

It's full bodied, and it's got that characteristic "Juicy" Neipa taste, but it's also relatively hoppy, and a little bitter.

I like Neipas, but I think, by not overdoing it, this really hits a sweet spot.

## Hadouken - Amplified IPA (7%)

![](/photos/TinyRebel-2023-11.jpg "Hadouken - Amplified IPA"){.right height=150px}

<div class="colour-emoji">⬇↘➡👊</div>

While this is strong, and it's undeniably an IPA, and it is fairly hoppy, it's not as offensively hoppy as I was worried it might be.

If the "Cooler than Being Cool" was kinda like a "IPA like Neipa", this is a "Neipa like IPA", in that while it's mostly hoppy, it's got a good chunk of Tropical Juiciness to it. 

Can claims "First created in a garage by Brad and Gaz back in their homebrew days, and if this is an accurate recreation of what they were brewing back then[^3], they were well ahead of the trend. 

While you can sort of tell that this is on the strong side, it feels like another beer with the potential to sneak up on you.

[^3]: I'd be willing to bet that it isn't, although I wouldn't be _a lot_.

## Hank - Session IPA (4.2%)

![](/photos/TinyRebel-2023-12.jpg "Hank - Session IPA"){.right height=150px}

Lightly hopped, crisp, and more than a little malty, super refreshing overall.

I'm not really sure this should count as an IPA, but it definitely counts as delicious.

Back of the can reads "Mardukas, The Legend", which is apparently a reference to a film that I have _not_ seen.

 
