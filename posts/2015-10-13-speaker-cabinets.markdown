---
title: Speaker Cabinets
social_image_url: images/social/speaker-cabinets.jpg
description: In which I talk about building a set of speaker cabinets with my Grandad
tags: Hardware
---

It is a truth universally acknowledged (or at least my opinion), that any person in possession of enough space for one, must be in want of an awesome Hi-Fi. The handful of electronics modules I took at university have left me with the feeling that could make such a Hi-Fi. This is almost certainly a mistake.

Because of this, I have vague plans to build an amplifier at some point in the future. If and when I build this amplifier, I’m going to need speakers to plug into it. Since I’m planning on building the amplifier, it seems reasonable to also build a housing for the speakers.

I mentioned this to my Dad over the summer. The logic was, if I’m building speaker cabinets, I’ll need a workshop, and he has a workshop, so I should try and build something before I move out. It turns out, if you say speaker cabinet to my Dad, he focuses on the word cabinet over the word speaker, and thinks the project is overly ambitious. He still thought the project was too ambitious after I explained I was planning something more like a box than a chest of drawers.

It was at this point he suggested I talk to my Grandad.

My Grandad is a very good carpenter, although you’d have a hard time getting him to admit it. So I was pleasantly surprised when he seemed very keen on the idea. I was even more delighted when he offered me the wood to build them with.

Before building anything, I had to come up with a design, and before coming up with a design, I had to choose electrical components. My component selection was heavily motivated by a desire not to spend very much money. I was, and still am, working under the assumption that I can replace parts of the set-up when I have the budget to do so. The components I used were:

* [A pair of these Visaton 6.5" 8Ω Mid Range Drivers](http://cpc.farnell.com/visaton/31065/speaker-6-5-full-range-driver/dp/LS01915?CMP=TREML007-005) (Actually a full range driver)
* [A pair of these non-brand 5.25" 8Ω woofers](http://cpc.farnell.com/unbranded/55-1205/speaker-woofer-5-1-4/dp/LS00411?CMP=TREML007-005)
* [A pair of these non-brand, but alarmingly large 8Ω tweeters](http://cpc.farnell.com/mcm-audio-select/53-1080/30w-soft-dome-tweeter/dp/LS04702?CMP=TREML007-005)
* [This pair of three way cross over filters](http://www.amazon.co.uk/dp/B00TDTDX8U/ref=pe_385721_37986871_TE_item)

I decided to do the design of the cabinets in [Blender](http://blender.org/). Blender is a really a Computer Graphics package, while what I really wanted was a CAD package. I went for this because I’d done a lot of other stuff in Blender recently, and couldn’t be bothered to find and learn a more suitable program. This meant I had to do a lot of calculations by hand, and the design files I ended up with were pretty difficult to modify. On the other hand, the renderings produced looked really pretty.

<iframe width="100%" height="529" src="https://www.youtube.com/embed/skCGLyF5gmo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](/photos/01-Plaining.jpg "I'm not sure that this qualifies as good technique"){.right width=30% }

Obviously no plan ever survives contact with the enemy (or in this case my Grandad), and what we wound out building wasn’t exactly like my diagrams. It was still incredibly helpful to have worked out lengths and angles in advance.

The first task was to prepare the beech that was used for the sides of the cabinets. This involved sanding, cutting roughly to size, and then planing down to the exact size. The beech we used was incredibly hard, so the planing took a fair bit longer than I would have anticipated. 

![](/photos/02-Partial-Dovetails.jpg "Some partially cut dovetails"){ .left width=40% }


After this we marked out and cut dovetails joints, I did about half of these by hand before we decided to use the bandsaw. You may remember from the design that the dovetails are not quite at right angles; they are at 95 degrees, so even once we’d decided to switch to the bandsaw we found that about half the cuts were at angles that had to be cut by hand. This was still pretty slow going. There was a fair amount of chiselling required, and since the beech was so hard, it was difficult to chisel quickly without causing damage. 


![](/photos/03-Clamping.jpg "Look at those sash clamps"){.right width=30% }

Once the dovetails were cut, the next stage was to glue the sides together. This came down to wood-glue, clamp and then leave for a few hours. Nothing fancy, except for the amazingly long sash clamps.

After clamping, we mixed some filler out of wood glue and sandings (wood dust), and used that to patch up the gaps in the dovetails. More filler was used than I would have liked. I’m putting this down to the difficulty of accurately shaping hard wood, especially with the chisel. The filler got onto the dovetails a little and left them looking less than perfect, but this would be removed later by sanding.

![](/photos/04-Oak.jpg "Oak pannels with circular holes"){ .right width=30% }

We cut sections of oak of the front of the speakers, and cut circular holes for each of the drivers. The oak was thick and strong enough that we had to cut the circles part of the way through with a router before we were able to get them out with a jig saw. In principle, I should have used a circle guide for the router, but I couldn’t figure out how to do this, so I wound out marking the circles out with a compass and cutting them by hand. This was fine, since these holes aren’t supposed to be visible.
The oak panels were glued onto the front of the beach boxes and clamped, this time using smaller F-clamps.

This was the point where what we were making started to deviate significantly from the design. The speakers have a section of plywood mounted to the front. These fit around the drivers, and are covered in fabric to protect the drivers from damage. In the original CAD these were joined to the body of the speakers using dowel. My Grandad’s suggestion was to have a strip of wood running around the outside of the plywood, forming a hole that the plywood pushes into. The strip of wood is mounted in a grove that was cut using the router. We went for a strip of walnut since this dark wood makes a pleasing contrast with the lighter woods used in the body. 


![](/photos/05-Router-Practice.jpg "Router Practice"){ .left width=40% }

The only real downside to this plan was that I wasn’t very good at handling the router. It took quite a number of practice grooves in test pieces of wood before I was confident enough to cut groves in the real thing. 


![](/photos/06-Grooves.jpg "Cleaning up the corners"){ .right width=40% }

Eventually I got confident enough, and we cut groves around the edge of the cabinet. We took a chisel to their corners to make them sharp, and also put a number of wood screws into the groves to hold the oak onto the beech body, since these would be hidden by the walnut. After this we cut the walnut to size and glued it into place. 


We also glued a couple of strips of wood into the inside of each speaker, near to the rear. This was so we had something to attach the back panel to. The back panel was cut out of thin plywood, and had a couple of small holes for bolts to use as electrical contacts, and one 50mm “port” hole to allow air flow.

The cabinet was finished by sanding with varying grades of sandpaper, and applying sanding sealer. I have to give my Grandad complete credit for this, as I was absent for it.

The protective plywood was cut to the correct size to fit the holes formed by the walnut. We also cut circular holes in it to match the drivers. Fabric was stretched over the plywood and stapled on. This was done with the help (and the fabric) of my Grandma. The staples and staple gun were borrowed from my Mum, who managed to staple herself in the stomach while demonstrating how to use it, all the more harrowing as she was in the process of booking herself onto a chainsaw course at the time.

![](/photos/07-Drivers.jpg "The drivers"){ .left width=30% }

![](/photos/08-Wiring.jpg "Wiring up the drivers"){ .right width=40% }

The drivers were fitted into place and attached using wood screws. Similarly the crossover filters were screwed to the base of the cabinets.

The drivers were wired to the crossover filters, and the crossover filters were wired to the bolts on the back panel. With the wiring complete the back could be screwed into place. 

Finally the plywood with the fabric cover was fitted to the front of the speakers, completing them. In some of the photos the right hand cover doesn’t look fully inserted, this has since been fixed. 

![](/photos/09-Final.jpg "The finished speakers"){ .inline width=48% }![](/photos/10-Final-Group.jpg "A team photo"){ .inline width=48% }

I’m entirely pleased with how these look, and frankly I don’t really have a good enough ear to find anything wrong with the sound. I do still have plans to replace the mid range drivers at some point in the future, but that won’t be happening any time soon.

Finally, I’ll leave you with a rendering from the updated CAD files, this matches the speakers that were actually built. 

<iframe width="100%" height="529" src="https://www.youtube.com/embed/vGHaX5bjHt8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
