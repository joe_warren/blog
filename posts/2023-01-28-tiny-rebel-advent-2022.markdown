---
title: Tiny Rebel Advent 2022
social_image_url: images/social/tiny-rebel-2022.jpg
description: In which I buy, drink and review the Tiny Rebel advent calendar for 2022, only slightly late
tags: Food
---

Much like [last year](2021-12-26-tiny-rebel-advent.html), I bought one of those "beer advent calendar" boxes, by [Tiny Rebel](https://www.tinyrebel.co.uk/).

Unfortunately, I had some kind of flu in the opening weeks of December, so I didn't get around to cracking the box open until January.

As with last year, these beers aren't available anymore, and also I don't have a particularly sophisticated palate, so I'd recommend you don't read this.

 
## №.1 - Triple Citra Tipa - 10%

![](/photos/TinyRebel-2022-01.jpg "Triple Citra Tipa"){.right height=150px}
![](/photos/TinyRebel-2022-01-can.jpg "Triple Citra Tipa"){.right height=150px}

A rich taste, slightly sweet; you can tell it's a 10% beer. 

But at the same time, heavily floral, almost spicy.

The can has gingerbread men on it, and while it's not a "novelty ginger beer" I don't think the ginger notes are just my imagination. 

It does the whole "flowery IPA" thing without being too much of an IPA; damn solid beer.

## №.2 - Damson Double Fruited Ale - 6.2%

![](/photos/TinyRebel-2022-02.jpg "Damson Double Fruited Ale"){.right height=150px}
![](/photos/TinyRebel-2022-02-can.jpg "Damson Double Fruited Ale"){.right height=150px}

Love a fruit beer, which probably colours my review.

Plum pudding; slightly sweet, just a little sharp.

Nose is heavy on the fruit, which hits the tongue first, leaving the richer, maltier notes to linger.

After №.1, 6% doesn't seem so strong by comparison, but it's still as full bodied and rich as you'd expect from a strong beer.

Colour is a  murky brown, like the flesh of a ripe plum. (It's possible that this has suffered from being left until after Christmas)

## №.3 - Galaxy IPA - 6.2%

![](/photos/TinyRebel-2022-03.jpg "Galaxy IPA"){.right height=150px}
![](/photos/TinyRebel-2022-03-can.jpg "Galaxy IPA"){.right height=150px}

Damn dry, crisp, floral (almost lavender), slightly bitter, fairly standard IPA turf.

It's the wrong time of year for something this refreshing.

## №.4 - Very Cherry Amaretto - Red Stout - 5.8%

![](/photos/TinyRebel-2022-04.jpg "Very Cherry Amaretto - Red Stout"){.right height=150px}
![](/photos/TinyRebel-2022-04-can.jpg "Very Cherry Amaretto - Red Stout"){.right height=150px}

Tastes of those plasticy glace cherries you get on a Bakewell tart.

Pleasantly sharp but slightly too light a taste for the intensity of the cherry.

It's alright as a novelty, and it's the kind of novelty that you want in an advent calendar, but would benefit from being a heavier stout with a less synthetic cherry flavour 

## №.5 - Neipa Table Beer - 2.2%

![](/photos/TinyRebel-2022-05.jpg "Neipa Table Beer"){.right height=150px}
![](/photos/TinyRebel-2022-05-can.jpg "Neipa Table Beer"){.right height=150px}

Effervescent to the point that I can almost taste the C02. Also crisp and hoppy, but not _too_ hoppy, fairly well balanced.

While you could legitimately complain about getting something with such a light alcohol content in an advent calendar, I've just finished putting a coat of paint onto a bathroom, and something this light and refreshing is _definitely_ called for.

## №.6 - Cryo Pop IPA - 4.8%

![](/photos/TinyRebel-2022-06.jpg "Cryo Pop IPA"){.right height=150px}
![](/photos/TinyRebel-2022-06-can.jpg "Cryo Pop IPA"){.right height=150px}

Very much an IPA, hoppy, refreshing but with more than a degree of bitterness. 

It's good, it's a damn fine floral hoppy flavour, with just a hint of sweetness to balance it, but it's also fairly typical craft beer fare; and as with 03, it's not exactly the season for this kind of beer.

## №.7 - Lil Saison IPA - 4.3%

![](/photos/TinyRebel-2022-07-can.jpg "Lil Saison IPA"){.right height=150px}

Another IPA, this one's fruity, I'm almost getting orange zest, it's good, kinda light and foamy too, which is nicer than it sounds.

I was worried that the IPAs were going to get to be a bit too much, but they haven't _yet_.

Can has a picture of a Christmas pudding, to which I say "not really". 

## №.8 - Vic Secret IPA - 5.8%

![](/photos/TinyRebel-2022-08.jpg "Vic Secret IPA"){.right height=150px}
![](/photos/TinyRebel-2022-08-can.jpg "Vic Secret IPA"){.right height=150px}

Regret to inform you that the taste of this beer doesn't remind me of the series of occult attempts to resurrect Prince Albert carried out in Osbourne House in the late 1860s, nor is it reminiscent of lacy pants.

Instead it's a rather bitter IPA, with the bitterness slightly offset by some sweetness. 

I'm not sure I rate it as highly as the previous two IPAs, but I enjoyed it.

## №.9 - Smokey Joe's Rauch Lager - 5%

![](/photos/TinyRebel-2022-09.jpg "Smokey Joe's Rauch Lager"){.right height=150px}
![](/photos/TinyRebel-2022-09-can.jpg "Smokey Joe's Rauch Lager"){.right height=150px}
Damn Smokey, also very damn "Lager". 

It's light, as you'd expect. The smokey notes hit _very late_, it's almost nutty, which contrasts with the initial lightness.

"Rauchbier" is a new concept for me. 

There's a picture of a chocolate log on the can, but the beer itself is not _wildly seasonal_.
It's also not the sort of thing I'd buy, but I'm enjoying it.


## №.10 - Chuckleberry - Holy Fuck Sour - 7.3%

![](/photos/TinyRebel-2022-10.jpg "Chuckleberry - Holy Fuck Sour"){.right height=150px}
![](/photos/TinyRebel-2022-10-can.jpg "Chuckleberry - Holy Fuck Sour"){.right height=150px}

Sour beer's are an acquired taste, but I really like them.

I remember the 2021 advent calendar sour's _weren't really sour enough_.
This on the other hand, is great.

The 2021 calendar had a Chuckleberry Radler in it, which was a contender for the best beer in that box; this is like the "grown up" version of that.

In the 2021 review, I wrote the following about the Chuckleberry Radler:

> Properly sharp, (a chuckleberry’s apparently part redcurrant, part gooseberry and part jostaberry), and aggressively red and fruity, it’s simultaneously electrifying and delicious, really delivers on those berry notes.

I could say all of that about this, and more, the little bit of extra punch it's packing goes a long way.

What do I think? It's very nice, It is very nice.

## №.11 - Christmas Tree Vienna Pale - 5.2%

![](/photos/TinyRebel-2022-11.jpg "Christmas Tree Vienna Pale"){.right height=150px}
![](/photos/TinyRebel-2022-11-can.jpg "Christmas Tree Vienna Pale"){.right height=150px}

Wow, that's resinous.

Tastes like biting into a real christmas tree.

Ingredients list has "spruce tips" on it.

It's like having flashbacks to a being in a pine forest in the snow, and getting my gloves gummed up with sticky sap.

Actual beer parts are good, it's very light, so the spruce flavour gets center stage.

This is damn weird, but I like it a lot.

It's absolutely the kind of thing I buy these calendars for.

"This is excellent, it's like drinking a tree" may seem like a hard sell, but I'm entirely on board.

## №.12 - Tropical Lil IPA - 3%


![](/photos/TinyRebel-2022-12.jpg "Tropical Lil IPA"){.right height=150px}
![](/photos/TinyRebel-2022-12-can.jpg "Tropical Lil IPA"){.right height=150px}

Smells almost entirely (but not quite) of pineapple.

Hell of a lot drier than I was expecting based on the smell.

It's simultaneously both sharp, and bitter, and it's fruitier on the nose than it is on the tongue.

Ingredients list reads "Tropical Juice", so I'm guessing there are more fruits in here than just pineapple.

Possibly a hint of mango and papaya, but I'd be misleading if I sounded confident about that.

All in all, I like this, the extreme dryness means it's not just another fruit beer.

However, the pine flavoured beer was easier to like, which is quite the sentence. 

## №.13 - Lotus DDH Cold Fermented IPA - 4.7%

![](/photos/TinyRebel-2022-13-can.jpg "Lotus DDH Cold Fermented IPA"){.right height=150px}

After a long run of "weird" beers, I was psyching myself up to be disappointed now we're back on the IPAs.

Not so, this is _nice_. 

I'd describe it as delicate, like when you finally get round to using the "expensive" tea that someone gifted you.

Aromatic, slightly sweet, slightly fruity, only a very little bitterness, but first and foremost, aromatic.

## №.14 - Veggie Turkey Soup - 4.5%

![](/photos/TinyRebel-2022-14-can.jpg "Veggie Turkey Soup"){.right height=150px}

This feels like the symbolic sibling of the "Bloody Mary" beer from last year.

It's kind of "salty".

Ingredients list contains "vegetable stock", but nothing to do with turkeys, and that comes across in the flavour, I'm getting more like "leaks", "celery" or "vegetable stock cube" than "turkey".

The "soupy" flavour kind of sneaks up on you, you take a sip, think "this basically tastes of beer", relax, and then suddenly there it is.

I'm all for having one or two zany beers in the box, but if I never drink another one of these, I'll be okay with that.

## №.15 - TDH IPA - 5.2%

![](/photos/TinyRebel-2022-15.jpg "TDH IPA"){.right height=150px}
![](/photos/TinyRebel-2022-15-can.jpg "TDH IPA"){.right height=150px}

Initially thought this was really bitter, but after the first few sips, it's not quite so dominant.

Very hoppy IPA, which I guess is covered by the "Triple" in triple dry hopped.

Fairly sweet, (but not to a fault), but the fragrance is where the money is.

I'm glad we've just had a bonkers "stock" flavour beer, because the earlier beers were _full_ of these fragrant IPAs, which, while delicious, would blur into one another a little bit.

## №.16 - Sauvin Blanc Dipa - 7%

![](/photos/TinyRebel-2022-16-can.jpg "Sauvin Blanc Dipa"){.right height=150px}

Initial thought is that this smells _exactly_ like white wine.  

This was _upon opening the can_, it hit even before pouring.

Upon drinking, the resemblance to wine dials back from "exactly" to "almost exactly".

You can tell that it's a wine yeast, but also it's got bunch of "grapey" notes. 

I'm fairly sure that the effervescence gives away that it's "beer fizz" and not "sparkling wine", as does a _hint_ of hoppiness.

That said, I reckon you could serve this to me and pass it of as sauv blanc, especially if I wasn't primed for it.

I will admit, I'm drinking this, and thinking "This is very technically impressive" a lot more than I'm thinking "This is delicious".

It's pretty good though, would drink again, and it's an excellent novelty beer.

## №.17 - Trifle White Stout - 6.4%

![](/photos/TinyRebel-2022-17.jpg "Trifle White Stout"){.right height=150px}
![](/photos/TinyRebel-2022-17-can.jpg "Trifle White Stout"){.right height=150px}

It's another "Beer that tastes like a thing".
This one's a pretty accurate recreation of a trifle.

Colour's fairly aggressively red in a way that replicates the colour of the fruit layer pretty well.

Ingredients list doesn't mention fruit, but I'm 100% confident there's raspberry in this (even more so after watching the companion video).
I initially thought gooseberry as well, but I'm pretty sure that's an illusion from the combination of raspberry and beer.

The nose is fine, but the best part of this beer hits about a second after it hits the tongue.

The initial fruitiness is balanced by a rich creaminess, like the fusion of fruit, booze and custard in a real trifle; and the end result is damn good.

I'm pretty sure is my first time coming across whatever a "white stout" is, and it's a very pleasant introduction.

## №.18 - Mango & Peach Melba Lil Froozie - 4%

![](/photos/TinyRebel-2022-18.jpg "Mango and Peach Melba Lil Froozie"){.right height=150px}
![](/photos/TinyRebel-2022-18-can.jpg "Mango and Peach Melba Lil Froozie"){.right height=150px}

This is intensely fruity, more peach than mango, but both fruits come across.

I like a fruit beer, but this is a fruit combination that suggests "smoothie" rather than "fruit beer".

Apart from being fruity, it's also simultaneously quite tart and bitter, and relatively dry for something this fruity.

Fairly low carbonation too, neither a lot of head, or a lot of fizz. 

Very murky colour too, which I'm assuming comes from the fruit.

Not horrible, and it's unambiguously an interesting drink, but while the fruit flavour itself is good, this is far from my favourite beer in the box.

## №.19 - Coconut Cream Stout - 5%

![](/photos/TinyRebel-2022-19.jpg "Coconut Cream Stout"){.right height=150px}
![](/photos/TinyRebel-2022-19-can.jpg "Coconut Cream Stout"){.right height=150px}

As a big fan of both dark beer, and coconut cream, this is very relevant to my interests.

It's super dark, has a powerful coconut aroma, and is damn rich and creamy.

The coconut is _almost_ too much, I feel like I'm in the process of making a curry.

Fortunately, I _enjoy_ the process of making a curry.

Full bodied, rich, smooth, one big simple novelty, well executed: this is an excellent beer.

## №.20 - Citra & Pink Lemonade Pale Ale - 4%

![](/photos/TinyRebel-2022-20.jpg "Citra & Pink Lemonade Pale Ale"){.right height=150px}
![](/photos/TinyRebel-2022-20-can.jpg "Citra & Pink Lemonade Pale Ale"){.right height=150px}

This is _tangy_, 2022 seems to have been the year that Tiny Rebel learned how to make a _real_ sour beer [^1].

Strong citrus taste, specifically lemon, if I'm being honest, There's clearly a bunch of fruit in here, too, which explains the "pink". 

It's so sharp, it's maybe a little bit hard to pick up on anything that the "beer" is doing, almost like someone made a shandy, but substituted the lemonade for lemon juice.

It's so sharp, you could almost imagine pouring this onto sugar and eating it off a crepe.

I don't want to do it an injustice though, like most very sour beers, it develops as you adapt to to it. And the hoppy flavours are there, but it's a fight to pick them up, and I am enjoying it, it's just very intense. 

[^1]: Actually, I'm fairly sure that this _isn't_ a _real_ sour, in the "it's been kettle soured" sense, but it is _really sour_

## №.21 - Cashmere IPA - 6%

![](/photos/TinyRebel-2022-21.jpg "Cashmere IPA"){.right height=150px}
![](/photos/TinyRebel-2022-21-can.jpg "Cashmere IPA"){.right height=150px}

We're back on the IPAs; this is a good one.

Crisp, fizzy and fruity (somewhere between citrus and pineapple), and with the satisfyingly full body that you get from a strongish IPA.

On the first sip, I thought this was going to be refreshing, and was all cued up to label it non seasonal, but I think the alcohol content carries it. 

## №.22 - Chocolate & Raspberry Coolie - 8.6%

![](/photos/TinyRebel-2022-22.jpg "Chocolate & Raspberry Coolie"){.right height=150px}
![](/photos/TinyRebel-2022-22-can.jpg "Chocolate & Raspberry Coolie"){.right height=150px}

Dark, to the point of bitterness, and _potent_ with both the tastes of coco and raspberry.

It can't be denied that this is delivering on it's name.

There's more than a little bit of sweetness, without which, the bitterness would be _way_ too much.

With that sweetness, though, it's delicious.

Basically like drinking a desert.

The "Stout", "Chocolate" and "Raspberry" components each complement one another _really_ well.

Stunning beer.

## №.23 - The Negroni - 8%

![](/photos/TinyRebel-2022-23.jpg "The Negroni"){.right height=150px}
![](/photos/TinyRebel-2022-23-can.jpg "The Negroni"){.right height=150px}

Undeniably tastes like a Negroni; Blood orange, bitter campari, botanicals from the Gin and vermouth all there.

Similar to the Sauvignon Blanc beer, in that I'd probably describe this as more "technically impressive" than objectively delicious.

There's an element of the "cover song" problem, where this is reminding me of a different band/drink that I could be consuming instead.

I _think_ I like this, though.

I keep flip flopping on whether or not the bitter campari taste works as a beer, and I'm still a little undecided whether this is strong enough to pull it off. 

On balance, I think it comes through, and it's indisputable that the orange notes work really well.

## №.24 - Triple Eggnog Dessert Beer - 7.2%

![](/photos/TinyRebel-2022-24-can.jpg "Triple Eggnog Desert Beer"){.right height=150px}

Slightly yeasty, in a way that makes me feel like I might be responsible for getting to the calendar after it's best before date.

Does hit the eggnog taste: elements of christmas spices, vanilla, caramel. 

Lot of sweetness, which pairs well with the spice, and damn heavy; tastes like a seven point two percenter.

There's more than a hint of acidity too, which gives it a sense of fruitiness, and stops the heavy sweetness from being too much.

I'm suspicious that this is a much nicer beer that's not stored well, which is a let down, it's not badly off, but I don't feel like these yeasty notes are "right".
 
## №.25 - Tiramisu Imperial Stout - 9%

![](/photos/TinyRebel-2022-25.jpg "Tiramisu Imperial Stout"){.right height=150px}
![](/photos/TinyRebel-2022-25-can.jpg "Tiramisu Imperial Stout"){.right height=150px}

Merry Christmas, or in my case Merry "Wednesday the 25th of January".

Love an Imperial Stout.

This one's going pretty hard on the "Tiramisu" theme; specifically the spongecake soaked in coffee and booze.

Really heavy, really rich, stout flavour still comes through even though there's so much coffee in it.

It's a damn good coffee too, mellow, really complements the stout.

Top notch beer to close the box on (if maybe a little powerful for a Wednesday).

