---
title: About
---
My name is Joe, I'm a Software Developer, and Engineering graduate , based in the UK.

I enjoy Scala, Haskell, Functional Programming, Data Visualization and Cycling. But not necessarily in that order.

In my spare time, I make my own shirts. 

[I'm on Twitter.](https://twitter.com/hungryjoewarren)

[Most of the code I write recreationally ends up on BitBucket.](https://bitbucket.org/joe_warren/)

[When I design for 3d Printing, it usually ends up on Thingiverse.](https://www.thingiverse.com/hungryjoe/designs)