import qualified Waterfall 
import Linear 
import Data.Function ((&))

object :: Waterfall.Solid
object = 
    let cubeBelow = 
            Waterfall.centeredCube &
                Waterfall.translate (-0.5 *^ unit _z) &
                Waterfall.uScale 3 
        removeBottom = (`Waterfall.difference` cubeBelow)
        dome = removeBottom $ Waterfall.unitSphere
        brim = 
            Waterfall.prism 0.05 $ Waterfall.fromPath $ Waterfall.pathFrom (V2 0 1)
                [ Waterfall.lineTo (V2 1 1)
                , Waterfall.bezierTo (V2 1.6 1) (V2 1.75 0.4)(V2 1.75 0)
                , Waterfall.bezierTo (V2 1.75 (-0.4)) (V2 1.6 (-1)) (V2 1 (-1))
                , Waterfall.lineTo (V2 0 (-1))
                , Waterfall.lineTo (V2 0 1)
                ]
        headspace = Waterfall.uScale 0.95 Waterfall.unitSphere
        bandgap = 
            Waterfall.centeredCylinder &
                Waterfall.scale (V3 0.3 0.4 1) &
                Waterfall.rotate (unit _y) (pi/2) &
                removeBottom &
                Waterfall.translate (V3 (-1) 0 0.1)
        in  mconcat [dome, brim] `Waterfall.difference` mconcat [headspace, bandgap]

main :: IO ()
main = Waterfall.writeSTL 0.01 "cap.stl" object


