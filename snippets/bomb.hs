import qualified Waterfall
import qualified Waterfall.Solids as Solids
import qualified Waterfall.Transforms as Transforms
import Linear
import Data.Function ((&))

bomb :: Waterfall.Solid 
bomb = 
    let sphere = Waterfall.translate (negate $ unit _z) Waterfall.unitSphere
        cyl = Waterfall.uScale 0.25 Waterfall.centeredCylinder
        fusePath = Waterfall.bezier3D zero (V3 0 0 0.6) (V3 0 0.5 0.5) (V3 0 0.75 0.75)
        fuse = Waterfall.sweep fusePath (Waterfall.uScale2D 0.075 Waterfall.unitCircle)
        blast = 
            Waterfall.centeredCube &
                Waterfall.translate (unit _y) &
                Waterfall.scale (V3 0.1 0.25 0.1) & 
                iterate (Waterfall.rotate (unit _x) (2* pi/5)) &
                take 4 & 
                mconcat & 
                Waterfall.rotate (unit _x) (16 * pi/10) &
                Waterfall.translate (V3 0 0.75 0.75)
        font = Waterfall.fontFromSystem "monospace" Waterfall.Regular 0.6
        text = Waterfall.prism 5 $ Waterfall.text font "c++"
        textOnSurface =
            (Waterfall.unitSphere `Waterfall.intersection` text) &
                Waterfall.uScale 1.05 &
                Waterfall.rotate (unit _x) (pi/2) &
                Waterfall.rotate (unit _z) (pi/2) &
                Waterfall.translate (negate $ unit _z)
     in mconcat [sphere, cyl, fuse, blast, textOnSurface]

main :: IO ()
main = do 
    Waterfall.writeSTL 0.001 "bomb.stl" (bomb)