import qualified Waterfall 
import Linear 
import Data.Function ((&))

renderText :: String -> Waterfall.Solid
renderText t = 
    let font = Waterfall.fontFromSystem "monospace" Waterfall.Regular 0.15
     in Waterfall.text font t &
            Waterfall.prism 0.1 &
            Waterfall.rotate (unit _x) (pi/2) 

object :: Waterfall.Solid
object = let box = Waterfall.scale (V3 175 14 14) Waterfall.centeredCube
             nameOne =
                renderText "c++" &
                    Waterfall.translate (0.1 *^ unit _z) &
                    Waterfall.rotate (unit _z) 0.2
             and = renderText "&"
             nameTwo =
                renderText "haskell" &
                   Waterfall.translate (-0.1 *^ unit _z) &
                   Waterfall.rotate (unit _z) (-0.2)     
         in  mconcat [nameOne, and, nameTwo]

main :: IO ()
main = Waterfall.writeSTL 0.01 "cpp-plus-hs.stl" object


