---
title: Egg Nog Sauce For Christmas Pudding
author: Granny Green 
---

## Ingredients 

* ½ Cup Sugar
* 2 Eggs
* 1 Cup Double Cream 
* Pinch Salt
* ½ tsp Vanilla
* Rum, Whisky or Brandy

## Method

Gradually add ¼ cup sugar into 2 well beaten egg yolks. 

Beat 2 whites until they stand in peaks.

Gradually add another ¼ cup sugar, and a pinch of salt.

Fold into egg yolk mixture.

Whip 1 cup heavy cream until stiff.

Flavour with ½ tsp vanilla.  

Add rum, whisky, or brandy, stirring in as little, and as lightly as possible.

