---
title: Coconut Macaroons
author: Great Grandma Warren (Grandad's Mum)
---

## Ingredients 

* 2 Egg Whites 
* 180g Sugar
* 2 teaspoons Vanilla
* 1 teaspoon Almond Essence
* 1 cup Desiccated coconut

## Method

Preheat oven to 150°C.

Beat egg whites and sugar on high speed with electric mixer for about 5 minutes.

Add vanilla and almond essence and beat well.

Mix in desiccated coconut.

Use an egg cup to form into mounds and place on greased baking sheets.

Bake for 25 mins or until golden brown.

