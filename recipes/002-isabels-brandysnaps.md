---
title: Isabel's Brandysnaps
author: Aunty Isabel
---

## Ingredients 

* 1 breakfast cupful each of:
    * Quaker Oats
    * Dessicated Coconut
    * Sugar
    * Plain Flour
* 6oz Butter
* 1 Tablespoon Golden Syrup 
* 1 tsp Bicarbonate of Soda
* 2 Tablespoon Water

## Method

Melt golden syrup and butter.

Add to dry ingredients.

Put cold water on bicarb, then add.

Put small teaspoonfuls on tins.

Touch with water.

Cook at 190°C for 5 to 10 minutes, until golden.

