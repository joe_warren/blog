---
title:  Cranachan
author: Monica
---

## Ingredients 

* 2 tbsp Medium Oatmeal
* 300g Fresh Raspberries
* Caster Sugar
* 350ml Double Cream
* 2 tbsp Honey
* 3 tbsp Whisky

## Method

Toast oatmeal until it smells rick and nutty. 
Leave to cool.

Crush half the raspberries and sieve.
Sweeten to taste.

Whisk double cream until just set.
Stir in honey and whisky.

Stir in oatmeal and whisk lightly until just firm.

Alternate layers of cream with remaining raspberries and purée in 4 dishes + chill slightly.
