---
title:  Debby and Colin's Crumble 
author: Debby and Colin 
---

## Ingredients 

* Topping 
    * 150g Plain Flour
    * 75g Butter
    * 30g Porridge Oats 
    * 75g Sugar, Brown[^1] or White 

[^1]: Better

## Method

Rub butter into flour.

Add oats and sugar.

Chop fruit into a dish. 
(Apples, rhubarb, etc)

Sprinkle  with sugar.

Sprinkle topping over.

Cook at 180°C for 30 mins or until nicely browned.

