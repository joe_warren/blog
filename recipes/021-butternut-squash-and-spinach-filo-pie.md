---
title: Butternut Squash and Spinach Filo Pie
author: Nerys
---

## Ingredients

* 1 Butternut Squash Cut Into 2cm Dice 
* 2 Red Onions
* 1 tsp Chilli Flakes
* 400g Spinach
* 100g Feta Cheese
* Handful Chopped Walnuts
* 4 Sheets Filo Pastry
* 1 tbsp Olive Oil

## Method

Put squash and onions in an ovenproof bowl and cook for 20 mins at 200°C until tender.

Pour boiling water over spinach and squeeze out excess liquid.

Mix spinach into squash.

Scatter over feta, chilli flakes and walnuts.

Scrumple up filo pastry and brush with olive oil.

Return to oven and cook for a further 15 mins until pastry is golden and crisp.

