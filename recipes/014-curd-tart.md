---
title: Curd Tart
author: Great Grandma Cutts 
---

## Ingredients 

* Shortcrust Pastry
* Equal Quantities of:
    * Curds [^1]
    * Margarine
    * Eggs
    * Sugar 
    * Currants

## Method

Line pie dish with pastry.

Cream marg. and sugar.
Whisk in eggs then add everything else.

Pour onto pastry and bake at ~180°C until done.


[^1]: Make curds by adding lemon juice to full fat milk, and strain off curds once formed.

