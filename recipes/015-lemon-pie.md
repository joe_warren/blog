---
title: Lemon Pie
author: Great Great Grandma Parkin (Grandma Warren's Grandma)
---

## Ingredients 

* Shortcrust Pastry
* 70oz Sugar 
* 3 oz Margarine
* 3 Eggs
* 2 Lemons. Zest and Juice

## Method

Line a pie dish

Cream marg. and sugar and whisk in eggs.
Add lemon juice and zest.

Pour into pastry and bake at 180°C until set and pastry golden.

