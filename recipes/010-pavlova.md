---
title: Pavlova
author: Aunty Jac
---

## Ingredients 

* 3 Egg Whites
* 6oz Caster Sugar
* 1 tsp Vinegar
* 1 level tsp Cornflour
* ½ pint Whipped Whipping Cream
* 8oz Fresh Raspberries
* A Little Caster Sugar to Sweeten

## Method

Lay a sheet of baking paper on a baking tray.
Mark an 8 inch circle on it.
Heat the oven to 180°C (150°C Fan).

Whisk the egg whites with an electric whisk until they are stiff.
Then whisk in the sugar a teaspoon at a time.
Blend the vinegar with the cornflour and whisk it into the egg whites with the last spoonful of sugar.

Spread the meringue out to cover the circle on the baking tray.
Building up the sides so they are higher than the center.

Put the meringue in the center of the oven.
Turn the heat down to 140°C and bake for 1 hour.

The pavlova will be a bale creamy color rather than white.

Turn the oven off and leave the pavlova undisturbed to become quite cold.
Lift off the paper and then place on a serving dish.

Put the cream and the raspberries in a bowl.
Lightly fold them together and the add a little sugar to sweeten to taste.

Pile cream and raspberry mixture into the center of the pavlova and leave to stand for 1 hour in a cool place or refrigerator before serving.

