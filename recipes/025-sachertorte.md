---
title:  Sachertorte
author: Cathryn
---

## Ingredients 

* Torte
    * 200g Dark Chocolate
    * 6 Eggs
    * 310g Granulated Sugar
    * 150g Ground Almonds
    * 1½ tsp Ground Coffee
    * Apricot Jam
* Icing 
    * 100g Dark Chocolate
    * 40g Butter

## Method

Preheat oven to 180°C, and brush 23cm tin with melted butter and line with greaseproof paper.

Melt chocolate  in heatproof bowl over simmering water

Separate 5 eggs.
Whisk the whites to stiff peaks.

In a separate bowl whisk yolks and whole egg with sugar until they are creamy.

Add ground almonds, melted chocolate and coffee grounds to egg yolk mixture and stir in.

Gently fold whites into mixture.

Pour into tin and bake for around 1 hour, covering with foil after 40 mins to prevent burning.

Turn out onto a wire rack and leave to cool.

Spread strained apricot jam over.

----

To make icing, melt remaining chocolate with butter and pour over cake spreading it over top and sides.
