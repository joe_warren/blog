---
title: Cinnamon Apple Cake
author: Uncle Don
---

## Ingredients 

* 210g Plain Flour
* 2 tsp Baking Powder
* ½ tsp Baking Soda
* ¼ tsp Salt
* 150g Caster Sugar
* 1 Large Apple. Peeled and roughly chopped.
* 2 Large Eggs
* 1 tsp Vanilla Extract
* 120ml Vegetable Oil
* 120ml Greek Yogurt
* Topping:
    * 50g Caster Sugar
    * 1 tsp Ground Cinnamon
    * 1 tablespoon Melted Butter
* Sugar Glaze:
    * 30g Icing Sugar (Sifted)
    * 1-2 tablespoons Milk

## Method

Preheat oven to 180°C/160°C Fan

Grease and line 8 inch round cake pan

Mix flour, baking powder, baking soda, salt and sugar.
Add chopped apple and stir briefly to combine.

In a separate bowl, add eggs, vanilla, oil and yogurt. 
Whisk briefly to break up egg yolks.

Add wet ingredients to dry ingredients and gently mix with a wooden spoon until combined.
Be careful not to overmix.
Spoon batter into prepared tin.

To make cinnamon topping.
Combine cinnamon sugar and butter. 
Mix together until lumpy and wet. Sprinkle over cake batter.

Bake cake for 30-35 mins until a skewer inserted into the middle comes out clean.
Transfer to a cake rack and cool completely.

To make the glaze, mix together sugar and 1 tablespoon of milk.
Stir until smooth.
If the glaze is too thick, add more milk.
Drizzle glaze over the cake.

