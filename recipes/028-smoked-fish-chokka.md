---
title:  Smoked Fish Chokka
author: John
---

## Ingredients 

* Tomatoes
* Chilli Pepper
* Onion
* Strongly Dry Smoked Fish

## Method

Peel tomatoes by dripping in boiling, then icy, water.

Finely dice tomato, onion, chilli and fish and mix together.

