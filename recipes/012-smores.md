---
title: S'mores
author: Kristen
---

Toast a marshmallow over a campfire and sandwich between two chocolate digestives, with the chocolate on the inside.
