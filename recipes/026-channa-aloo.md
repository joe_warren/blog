---
title: Channa Aloo
author: John
---

## Ingredients 

* Curry Powder
* Onion
* Garlic 
* Chickpeas
* Potatoes
* Kuchela

## Method

Fry grated onion and garlic pulp in oil with 2× tsp curry powder.

Add a tin of chickpeas and several floury potatoes.
Cover with water and boil until potatoes are cooked.

Add kuchela to taste.


