---
title: Daal
author: Jak
---

Serves 8-10

## Ingredients 

* 300g Whole (Black) Urad Daal
* 12g Garlic Paste
* 10g Ginger Paste
* 70g Tomato Puree
* 8g Salt 
* ⅔ tsp Deggi Mirch Chilli Powder
* ½ tsp Garam Masala
* 90g Unsalted Butter
* 90ml Double Cream

## Method

Rinse the daal.
Cook for 2-3 hrs in 4 litres water.
Skim off any impurities.
Add boiling water to keep grains covered.
Grains would be completely soft with skins coming away and creamy rather than crumbly.
Turn off heat and leave to sit for 15 mins.

Mix garlic + ginger paste, tomato purée, salt, chilli powder and garam masala into a paste.

Pour cooking water off daal and pour on enough freshly boiled water to cover the daal by 3-4 cm.
Bring to the boil over a medium-high heat and add the aromatic paste and butter.

Cook rapidly for 30 mins stirring regularly to prevent the mixture from sticking.

Lower the heat and simmer for 1-1½ hours, stirring very regularly to prevent it from sticking and adding a little boiling water if the liquid level gets near the level of the grains.
Eventually it will become thick and creamy.

Add the cream and cook for a further 15 mins.
Serve with Jak's Flatbread.

# Jak's Flatbread

70% ratio dough (eg. 70ml water for every 100g flour).

Couple teaspoons yeast, pinch of salt and plenty of raisins.

Mix all together.

Leave for 2 hours.

Roll out and fry in butter.

