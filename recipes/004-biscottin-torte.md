---
title: Biscottin Torte 
---

I [^1] knew this as biscuit + torte. A staple of family parties at Granny Scarratt's or Aunty Isabel's.

[^1]: Cathryn

## Ingredients 

* 40 Sponge Fingers
* 6oz Butter 
* 1 Egg
* 4oz Walnuts
* 1oz Flour
* 1oz Marg.
* ¼ pint Milk
* 1-2 tspn Instant Coffee

## Method

Beat butter and sugar with whole egg until creamy.

Mince and add nuts.

Melt marg. on low heat, add flour.

Let this cook gently for ½ minute then add milk.

Stir for 2-3 mins until it thickens to a consistency for a thick sauce and boils up once or twice.

Remove from heat, stir until cool.

Add a spoonful at a time to the cream.

Stir in the coffee and the mixture is ready.

---

Line a cake tin with greaseproof paper.

Dip sponge fingers in milk and sherry.

Line bottom and sides with them.

Put in alternate layers of cream and dipped sponge fingers finishing with sponge fingers.

Cover with greaseproof paper.

Put in a fridge for a few hours.

Decorate with cream and chocolate.

