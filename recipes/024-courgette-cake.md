---
title: Courgette Cake
author: Aunty Jac
---

Dairy free for Dad.

## Ingredients 

* 200g Grated  Courgette
* 200g Dark Brown Sugar
* 2 Large Eggs
* 200g Self Raising Flour
* 200ml Sunflower Oil
* Lemon Zest
* Icing Sugar

## Method

Line 2lb loaf tin and heat oven to 180°C.

Beat oil, sugar and eggs until smooth.

Slowly add flour, baking powder, and once mixed add courgette and lemon zest.

Pour into loaf tin and bake for 50 mins.

Make a frosting with the lemon juice and sieved icing sugar and pour over.

