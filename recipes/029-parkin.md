---
title: Parkin
author: The Parkin Family (Grandma's Mum's Family)
---

## Ingredients 

* 350g Medium Oatmeal
* 175g Plain Flour
* 1 level Tablespoon Caster Sugar
* ½ tsp Ginger
* 100g Margarine
* 450g Black Treacle
* 5 tbsp Milk
* ½ teaspoon bicarbonate of soda.

## Method

Heat oven to 180°C.
Grease and line 11×7×2 inch tin.

Put oatmeal, flour, sugar + ginger in a bowl.

Warm treacle and margarine gently until margarine has melted.

Warm milk and stir in bicarb to dissolve and add to treacle mixture.

Make a well in dry ingredients and pour in treacle mixture

Pour into a tin and bake for 40 mins.

