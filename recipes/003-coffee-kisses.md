---
title: Coffee Kisses
author: Granny Scarratt (Suggested by Uncle Phippy) 
---

## Ingredients 

* 175g S.R Flour
* 75g Caster Sugar
* 75g Margarine
* 1 Egg, Beaten
* 2 tsp Instant Coffee, Dissolved in 1 tbsp Hot Water

## Method

Heat oven to 180°C.

Grease 2 baking trays.

Mix flour + sugar.

Rub in margarine.

Stir in egg and coffee essence.

Mix well.

Divide into balls the size of a walnut.

Place on baking trays and bake for 15 mins.

When cold sandwich into pairs with coffee butter icing.

# Coffee Butter Icing

## Ingredients 

* 50g Butter
* 100g Sieved Icing Sugar
* 2 tsp Coffee Essence

## Method

Cream fat. 

Gradually add icing sugar.

Add flavouring and mix.

