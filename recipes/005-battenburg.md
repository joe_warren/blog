---
title: Battenburg
author: Eleanor
---

## Ingredients 

* 175g Butter
* 175g Sugar
* 140g Self-raising Flour
* 50g Ground Almonds
* ½ tsp Baking Powder
* 3 Eggs
* 2 tsp Almond Extract
* Pink Food Colouring
* Marzipan

## Method

Cream the butter and sugar, add in the remaining ingredients and mix.

Split into two and add pink food colouring to half.

Divide between two lined loaf tins and bake for 30 mins at 180°C.

---

Leave to cool.

---

Cut each cake in half lengthwise and smother in "Jam of the Day".

Roll out marzipan as thick as desired.

Assemble the cakes in classic battenburg formation on the marzipan and wrap.
