---
title: Yorkshire Puddings
author: Grandma Warren
---

## Ingredients 

* 100g Plain Flour
* ¼ tsp Salt
* 3 Large Eggs
* 225 ml Milk
* Lard

## Method

Preheat oven to 220°C/200°C Fan.

Put a knob of lard into each hole of a 12 hole bun tray and place in oven for 5 mins.

Mix flour and salt in a bowl.
Make a well in the center.
Add the eggs and a little of the milk.

Mix in Yorkshire air with an angels wing
(if not available, use a wooden spoon or electric whisk) and gradually add the remainder of the milk.

Pour the batter evenly between the holes of the tin.

Cook for 20-25 minutes, until well risen and golden.

Serve immediately.

