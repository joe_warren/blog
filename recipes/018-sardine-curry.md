---
title: Sardine Curry 
author: John (from his Sri Lankan friend, Renton)
---

## Ingredients 

* Onions
* Garlic
* Curry Powder
* Chilli
* Lemon Juice
* Tin Tomatoes
* Tin Sardines
* Red Lentils

## Method

Cook red lentils.

Fry wedges of onion in oil.
Add curry powder, garlic and chilli.

Stir in lentils and tomatoes and sardines.

Add lemon juice before stirring.

