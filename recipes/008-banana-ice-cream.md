---
title: Banana Ice Cream
author: Eleanor
---

Slice ripe bananas.

Freeze. 

Defrost for 10 mins.

Blend until smooth.

Add cocoa powder for an instant chocolate ice-cream. 
