---
title: Kedgeree
author: Jac and Dave
---

## Ingredients 

* 10 oz basmati Rice
* 1 pint Fish Stock, or Water if You Don't Have It
* 2 Small  Shallots, Finely Chopped
* 1oz Butter
* 1 lb Smoked Haddock
* Enough milk to just cover the fish in a lidded frying pan
* 1 Bayleaf
* 4 Soft Boiled Eggs
* 4 tablespoons Single Cream 
* 1 tablespoon Curry Powder
* Small handful of Chopped Parsley

You can use leftover cooked rice as long as it hasn't been in the fridge too long

## Method

Cook the rice in the usual way.

Boil the eggs for 5 mins, then drain and bash each shell to prevent further cooking.

Fry the shallots in butter until soft and then ad the fish and cover with the milk.

Add the bayleaf, cover and simmer for about 5 minutes, or until it looks cooked through. 

Set aside.

Peel the eggs and cut in half lengthwise.

Break up the fish into fork sized pieces, discard the bayleaf and mix the fish and milk into the rice along with the cream and parsley (and the curry powder if you're using it).

Season with black pepper.
Put into a warmed serving dish, arrange the eggs around the dish.
Sprinkle with the remaining parsley.

