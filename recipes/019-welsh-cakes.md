---
title: Welsh Cakes
author: Original Minafon
---

## Ingredients 

* 225g Self Raising Flour
* 4oz Marg. or Butter 
* 2oz Caster Sugar
* 2oz Currants
* 1 Egg
* 2 tbsp Milk

## Method

Rub fat into flour.
Stir in sugar and currants.

Mix with eggs and milk to make a stiff dough.

Cut into rounds and cook on a greased griddle or frying pan.

Serve with butter.

