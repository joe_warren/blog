---
title: Trifle
author: Granny Scarratt (Suggested by Jayne)
---

## Ingredients 

* Jam Filled Swiss roll
* 2 tbsp Sherry
* 8oz Frozen Raspberries
* 1 Vanilla Pod
* 3 fl oz Whole Milk
* 1 pint Double Cream
* 3 Egg Yolks
* 3 oz Caster Sugar 
* 1 tbsp Cornflour
* 3 tbsp Raspberry Jam

## Method

Cut Swiss roll into ¾ in slices and arrange in base of trifle dish or glass bowl.
Drizzle over the sherry.

Heat the raspberries with the jam until fruit softens and the mixture is saucy.
Pour over the cake.

To make custard heat milk and vanilla pod (or essence) and ⅓ pint cream until bubbling and leave to infuse.

Whisk egg yolks, caster sugar and cornflour until smooth, whisk in hot milk mixture, return to pan and simmer with whisking until it thickens.

Leave to cool for 10 minutes.

Pour custard over raspberries and chill for 20 minutes until custard is just beginning to set.

Lightly whip remaining cream and spoon over custard.

Chill for two hours.

----

Granny would probably pour a raspberry jelly over the raspberries and cake and also use birds custard or a raspberry blancmange mix.

