---
title: Smoked Haddock Gratin
author: Eleanor
---

Serves 4

## Ingredients 

* 500g Leaf Spinach 
* Butter for Greasing
* 500g Smoked Haddock Skinned and Cut into Four Portions
* 12 Cherry Tomatoes
* Topping
    * 200ml Double Cream or Crème Fraîche
    * Juice ½ Lemon
    * 100g Cheddar Cheese

## Method

Heat oven to 180°C|160°C fan|gas 4.

Place the spinach in a large colander in a sink and carefully and slowly pour over a kettleful of hot water to wilt it.

Cool again under the cold tap, then squeeze out as much liquid from the spinach as possible.

Butter a medium grating dish.

Roughly chop the spinach and scatter evenly over the base of the dish.

Season the spinach lightly with salt and pepper and lay the haddock fillets, skinned side down on top.

Nestle the tomatoes among the haddock fillets.

Mix the ingredients for the topping in a small bowl and season with lots of pepper and a little salt.

Dollop and spread the mixture over the fish and spinach.

Bake for 30 mins until bubbling and golden.

---

The actual recipe has 2 chopped spring onions and a grate of nutmeg into the topping, then a handful of dried breadcrumbs scattered on top. 

But Ellie leaves them out.

