---
title: Bacon and Chestnut Scones
author: Cathryn
---

## Ingredients 

* 225g Self Raising Flour
* 1 level tsp Baking Powder
* 50g Soft Margarine
* 1 Egg (Beaten)
* Cooked Bacon (Chopped)
* Cooked Chestnuts (Chopped)

## Method

Rub fat into flour.

Mix in bacon and chestnuts.

Add egg and a little milk to make a soft dough.

Roll out to ½ inch thick and cut into rounds.

Brush with beaten egg and dip into Demerara sugar.

Bake at 220°C for ten minutes or until golden brown.

