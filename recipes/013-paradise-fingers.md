---
title: Paradise Fingers
author: Grandma Warren
---

## Ingredients 

* 6 oz Shortcrust Pastry
* 3 oz Marg.
* 4 oz Sugar
* 1 egg
* 4 oz Sultanas
* 1½ oz Ground Rice
* 1½ oz Ground Almonds
* Almond Essence
* Cherries Glace (2 oz)
* Nuts Chopped (1 oz)
* Raspberry Jam

## Method

Line a baking tray with the shortcrust pastry.

Spread with raspberry jam.

Sprinkle over sultanas, cherries and nuts.

Cream together marg. and sugar + whisk in an egg.
Fold in the rice, almonds and almond essence. Spread the mixture over the mixture in the baking tray.

Bake for ~30 mins at 180°C.


