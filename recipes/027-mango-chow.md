---
title: Mango Chow
author: John
---

## Ingredients 

* Unripe Mango
* Garlic
* Chilli
* Shado Beni / Coriander Leaves
* White Vinegar
* Salt + Pepper

## Method

Peal + thinly slice mango.

Chop garlic, chilli + shado beni + add shake over vinegar, season + stir together.

