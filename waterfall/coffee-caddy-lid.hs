#!/usr/bin/env stack
{- stack script --resolver lts-22.6 
    --package linear
    --package waterfall-cad
    --extra-dep waterfall-cad-0.4.0.0
    --extra-dep opencascade-hs-0.4.0.0
-}

-- short-description: Lid for a coffee grinder.
-- description: 
-- description: I own a Sage coffee grinder. 
-- description: 
-- description: A while back, I purchased a separate "bean caddy" for it, so I could conveniently swap between regular, and [novelty high-caffeine](https://cannonballcoffee.co.uk) beans.
-- description: 
-- description: It turns out, that when you buy a new sage caddy, it doesn't come with a lid, those are sold separately. 
-- description: 
-- description: I figure; that's a pain, but no biggie, I can print that. 
-- description: 
-- description: The lid uses plumber's tape to form a seal with the caddy. 
-- description: 
-- description: I think if I was being super serious about iterating on the design, I'd probably add a recess for an o-ring, but the plumbers tape _works_. 
--
--
-- image: /photos/coffee-lid-01.jpg

import qualified Waterfall
import Linear 


lid :: Waterfall.Solid    
lid = 
    let rInner = 143/2
        hInner = 5
        inner = Waterfall.scale (V3 rInner rInner hInner) $ Waterfall.unitCylinder
        rOuter = rInner + 3
        hOuter = 5
        outer = 
            Waterfall.translate (unit _z ^* hInner) $
                Waterfall.scale (V3 rOuter rOuter hOuter) $ 
                    (Waterfall.unitCylinder <> (Waterfall.translate (unit _z) Waterfall.unitSphere)) 
        rHook = 15
        tHook = 8
        rHookOuter = rHook + tHook
        hHook = 10
        wHookCube = rHookOuter * 2 
        lHookCube = rHook + tHook +hHook
        hookCube = 
            Waterfall.translate (unit _x ^* (0.5 * lHookCube)) $ 
                Waterfall.scale (V3 lHookCube wHookCube tHook) $ 
                    Waterfall.centeredCube
        hook = 
            Waterfall.translate (V3 0 (rOuter - rHookOuter) hInner) $
                    Waterfall.roundFillet 3 $
                         Waterfall.rotate (unit _y) (pi/2) $ Waterfall.translate (negate $ unit _x ^* lHookCube) $ 
                            (hookCube <> (Waterfall.scale (V3 rHookOuter rHookOuter tHook ) $ Waterfall.centeredCylinder)) `Waterfall.difference` 
                                (Waterfall.scale (V3 rHook rHook (tHook*2)) Waterfall.centeredCylinder)
    
    in  inner <> outer <> hook


main :: IO ()
main = do
    let stlRes = 0.1
    Waterfall.writeSTL stlRes "coffee-caddy-lid.stl" lid
    
