#!/usr/bin/env stack
{- stack script --resolver lts-22.6 
    --package linear
    --package waterfall-cad
    --extra-dep waterfall-cad-0.4.0.0
    --extra-dep opencascade-hs-0.4.0.0
-}

-- short-description: Model of a Cartoon Bomb, Labelled C++
--
-- description: Model of a Cartoon Bomb, Labelled C++
-- description: 
-- description: This was used as a "flavour image" for a [blog post](../posts/2024-01-23-ffi.html) 
-- description: on Haskell FFI

import qualified Waterfall
import qualified Waterfall.Solids as Solids
import qualified Waterfall.Transforms as Transforms
import Linear
import Data.Function ((&))

bomb :: Waterfall.Font -> Waterfall.Solid 
bomb font = 
    let sphere = Waterfall.translate (negate $ unit _z) Waterfall.unitSphere
        cyl = Waterfall.uScale 0.25 Waterfall.centeredCylinder
        fusePath = Waterfall.bezier3D zero (V3 0 0 0.6) (V3 0 0.5 0.5) (V3 0 0.75 0.75)
        fuse = Waterfall.sweep fusePath (Waterfall.uScale2D 0.075 Waterfall.unitCircle)
        blast = 
            Waterfall.centeredCube &
                Waterfall.translate (unit _y) &
                Waterfall.scale (V3 0.1 0.25 0.1) & 
                iterate (Waterfall.rotate (unit _x) (2* pi/5)) &
                take 4 & 
                mconcat & 
                Waterfall.rotate (unit _x) (16 * pi/10) &
                Waterfall.translate (V3 0 0.75 0.75)
        text = Waterfall.prism 5 $ Waterfall.text font "c++"
        textOnSurface =
            (Waterfall.unitSphere `Waterfall.intersection` text) &
                Waterfall.uScale 1.05 &
                Waterfall.rotate (unit _x) (pi/2) &
                Waterfall.rotate (unit _z) (pi/2) &
                Waterfall.translate (negate $ unit _z)
     in mconcat [sphere, cyl, fuse, blast, textOnSurface]

main :: IO ()
main = do
    font <- Waterfall.fontFromSystem "monospace" Waterfall.Regular 0.6
    Waterfall.writeSTL 0.01 "bomb.stl" (bomb font)
