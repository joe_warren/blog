#!/usr/bin/env stack
{- stack script --resolver lts-22.6 
    --package linear
    --package waterfall-cad
    --extra-dep waterfall-cad-0.3.0.0
    --extra-dep opencascade-hs-0.3.0.0
-}

-- short-description: Generates a Parallelepiped 
--
-- description: [A Parallelepiped is a three-dimensional figure, formed by six parallelograms.](https://en.wikipedia.org/wiki/Parallelepiped)
-- description:
-- description: It relates to a parallelogram just as a cube relates to a square

import Linear
import qualified Waterfall


-- | Generates a parallelepiped
-- with one point on the origin
-- and sides given by each of the arguments
parallelepiped :: V3 Double -> V3 Double -> V3 Double -> Waterfall.Solid
parallelepiped v1 v2 v3 = 
  let base = Waterfall.pathFrom zero
                [ Waterfall.lineTo v1
                , Waterfall.lineTo (v2 + v1)
                , Waterfall.lineTo (v2)
                , Waterfall.lineTo zero
                ]
   in Waterfall.loft (1e-6) [base, Waterfall.translate v3 base]
   


a :: V3 Double 
a = V3 0 0 1

b :: V3 Double 
b = V3  0 2 0.1

c :: V3 Double 
c = V3 3 0.2 0.1

main :: IO ()
main = Waterfall.writeSTL 0.01 "parallelepiped.stl" $ parallelepiped a b c
