#!/usr/bin/env stack
{- stack script --resolver lts-22.6 
    --package linear
    --package waterfall-cad
    --extra-dep waterfall-cad-0.4.0.0
    --extra-dep opencascade-hs-0.4.0.0
-}

-- short-description: Compound of Five Cubes
--
-- image: /photos/compound-of-cubes-01.jpg
-- image: /photos/compound-of-cubes-02.jpg

import qualified Waterfall
import Linear (V3 (..))

cubes :: Waterfall.Solid 
cubes = 
    let phi = (1 + sqrt 5) / 2
        axis = V3 1 phi 0
        angle = 2 * pi / 5
     in mconcat . take 5 . iterate (Waterfall.rotate axis angle) $ Waterfall.centeredCube

main :: IO ()
main = do 
    let stlRes = 0.1
    Waterfall.writeSTL stlRes "cubes.stl" cubes
