#!/usr/bin/env stack
{- stack script --resolver lts-22.6 
    --package linear
    --package waterfall-cad
    --package lattices
    --extra-dep waterfall-cad-0.4.0.0
    --extra-dep opencascade-hs-0.4.0.0
-}

-- short-description: Compound of Five Cubes
--
-- description: Compound of Five Cubes
-- description: 
-- description: This version of this script outputs a split version of the cubes,
-- description: for convinient printing without overhangs

import qualified Waterfall
import Linear 
import Algebra.Lattice
import Algebra.Lattice.Dropped

intersection :: (Functor f, Foldable f) => f Waterfall.Solid -> Waterfall.Solid
intersection = foldDropped Waterfall.nowhere id . meets . fmap Drop

cubes :: Waterfall.Solid 
cubes = 
    let phi = (1 + sqrt 5) / 2
        axis = V3 1 phi 0
        angle = 2 * pi / 5
     in mconcat . take 5 . iterate (Waterfall.rotate axis angle) $ Waterfall.centeredCube

mask :: Waterfall.Solid
mask = Waterfall.uScale 100 . Waterfall.translate (0.5 *^ unit _z) $ Waterfall.centeredCube

slot :: Waterfall.Solid
slot = Waterfall.rotate (unit _x) (pi/2) . Waterfall.scale (V3 (26/2) (26/2) 2.5) $ Waterfall.centeredCylinder

main :: IO ()
main = do 
    let stlRes = 0.1
    Waterfall.writeSTL stlRes "cubes.stl" cubes
    Waterfall.writeSTL stlRes "cubes-half.stl" (((Waterfall.uScale 50 cubes) `Waterfall.intersection` mask ) `Waterfall.difference` slot)
