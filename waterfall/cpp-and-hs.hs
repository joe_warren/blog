#!/usr/bin/env stack
{- stack script --resolver lts-22.6 
    --package linear
    --package waterfall-cad
    --extra-dep waterfall-cad-0.4.0.0
    --extra-dep opencascade-hs-0.4.0.0
-}

-- short-description: Text Reading C++ and Haskell
--
-- description: Text Reading C++ and Haskell
-- description: 
-- description: This was used as a "flavour image" for a [blog post](../posts/2024-01-23-ffi.html) 
-- description: on Haskell FFI

import qualified Waterfall 
import Linear 
import Data.Function ((&))

renderText :: Waterfall.Font -> String -> Waterfall.Solid
renderText font t = 
    Waterfall.text font t &
        Waterfall.prism 0.1 &
        Waterfall.rotate (unit _x) (pi/2) 

object :: Waterfall.Font -> Waterfall.Solid
object font = 
    let box = Waterfall.scale (V3 175 14 14) Waterfall.centeredCube
        nameOne =
            renderText font "c++" &
                Waterfall.translate (0.1 *^ unit _z) &
                Waterfall.rotate (unit _z) 0.2
        and = renderText font "&"
        nameTwo =
            renderText font "haskell" &
                Waterfall.translate (-0.1 *^ unit _z) &
                Waterfall.rotate (unit _z) (-0.2)     
    in mconcat [nameOne, and, nameTwo]

main :: IO ()
main = do 
    font <- Waterfall.fontFromSystem "monospace" Waterfall.Regular 0.15
    Waterfall.writeSTL 0.01 "cpp-plus-hs.stl" (object font)


